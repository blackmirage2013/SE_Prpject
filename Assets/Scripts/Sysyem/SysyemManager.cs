﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class SysyemManager : MonoBehaviour
{
    public static SysyemManager SM;
    public string UserEmail;
    public string UserName;

    public GameObject LoginMenu; //登入選單
    public GameObject ConferenceMenu; //會議選單
    public GameObject LoomMenu; //管理者選單
    public GameObject Menu_Window; //切換選單
    public GameObject Identity_Window; //身分選單
    public GameObject Message_Window; //訊息選單
    public GameObject RoomState_Window; //房間現在狀態選單
    [Header("Window")]
    public ListenerRoomInfo Listener_Window;
    public StaffRoomInfo Staff_Window;
    public HostRoomInfo Host_Window;
    private void OnEnable()
    {
        SM = this;
    }

    public void OnLoginSuccess(string name, string email, bool saveEmail)
    {
        UserName = name;
        UserEmail = email;
        LoginMenu.SetActive(false);
        LoomMenu.SetActive(true);
        if (saveEmail)
        {
            Email e = new Email(UserEmail);
            string json = JsonUtility.ToJson(e, true);
            System.IO.StreamWriter file = new System.IO.StreamWriter(Application.streamingAssetsPath + "/Email.json");
            file.Write(json);
            file.Close();
        }
        else
        {
            string F = Application.streamingAssetsPath + "/Email.json";
            if (System.IO.File.Exists(F))
            {
                System.IO.File.Delete(F);
            }
        }
    }


    public void Message_Window_Open()
    {
        Message_Window.SetActive(true);
    }
    public void RoomState_Window_Open()
    {
        RoomState_Window.SetActive(true);
    }

    public void OnClickStaff()
    {
        Menu_Window.SetActive(false);
        Identity_Window.SetActive(false);
        ConferenceMenu.SetActive(true);
        LoomMenu.SetActive(false);
    }
    public void OnClickManager()
    {
        Menu_Window.SetActive(false);
        Identity_Window.SetActive(false);
        ConferenceMenu.SetActive(false);
        LoomMenu.SetActive(true);
    }

    public void Menu_Window_Open()
    {
        Menu_Window.SetActive(true);
    }

    public void Identity_Window_Open()
    {
        Identity_Window.SetActive(true);
    }


    public void ExitProgram()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}

[Serializable]
public class Email
{
    public string email;
    public Email(string e)
    {
        email = e;
    }
}