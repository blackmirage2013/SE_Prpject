﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeetingManager : MonoBehaviour
{
    public GameObject rowPrefab;
    public Transform context;
    [SerializeField] private CreateMeeting createMeeting;
    [SerializeField] private DatePicker datePicker;
    [SerializeField] private MeetingBox meetingBox;

    void Start()
    {
        RefreshContentButton();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable()
    {

    }

    private void OnDisable()
    {

    }
    public void RefreshMeetingBox(GameObject _go, int _input)
    {
        foreach (var _meetingData in RoomManager.instance.rooms[_input].GetComponent<MeetingRoom>().MeetingList)
        {
            if (_meetingData.meetingTime.startTime.Date <= datePicker.Date.date && _meetingData.meetingTime.endTime.Date >= datePicker.Date.date)
            {
                Instantiate(meetingBox, _go.transform).GetComponent<MeetingBox>().SetMeetingData(_meetingData);
            }
        }
    }
    public void RefreshCreateRoomButton(GameObject _go,int _input)
    {
        for (int i=1;i<25; i++)
        {
            DateTime dateTime =new DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day,i-1,0,0);
            _go.transform.GetChild(i).GetComponent<Button>().onClick.RemoveAllListeners();
            int tempInt = _input;
            _go.transform.GetChild(i).GetComponent<Button>().onClick.AddListener(() => createMeeting.RefreshPanel(dateTime));
            _go.transform.GetChild(i).GetComponent<Button>().onClick.AddListener(() => createMeeting.SetRoomSelectName(tempInt));
        }
    }
    public void RefreshContentButton()
    {
        for (int i = 1; i < context.childCount; i++)
        {
            Destroy(context.GetChild(i).gameObject);
        }
        for (int i = 0; i < RoomManager.instance.rooms.Count; i++)
        {
            GameObject tmp = Instantiate(rowPrefab, context);
            tmp.GetComponent<RowInfo>().Init(RoomManager.instance.rooms[i].GetComponent<MeetingRoom>());
            RefreshCreateRoomButton(tmp,i);
            RefreshMeetingBox(tmp, i);
        }
    }
}
