﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaffRoomInfo : MonoBehaviour
{
    [HideInInspector]
    [SerializeField] private MeetingReserve meetingReserve;
    [SerializeField] private Text meetingName;
    [SerializeField] private Text roomSelect;
    [SerializeField] private Text maxMemberAmount;
    [SerializeField] private Text meetingInfo;
    [SerializeField] private Text timeStart;
    [SerializeField] private Text timeEnd;
    [SerializeField] private Transform memberSlot;
    [SerializeField] private Transform inviteSlot;
    [SerializeField] private GameObject UserInfo;
    [SerializeField] private UpdateMeetings updateMeetings;
    [SerializeField] private meetingInfoLoader meetingInfoLoader;
    [SerializeField] private DataLoader userInfoLoader;
    private MeetingData meetingData;

    public void SetInfo(MeetingData _meetingData)
    {
        for (int i = memberSlot.childCount - 1; i >= 0; i--)
            Destroy(memberSlot.GetChild(i).gameObject);
        for (int i = inviteSlot.childCount - 1; i >= 0; i--)
            Destroy(inviteSlot.GetChild(i).gameObject);
        foreach (string temp in _meetingData.invite)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, inviteSlot).GetComponent<AccountInfo>().Copy(info);
        }
        foreach (string temp in _meetingData.member)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, memberSlot).GetComponent<AccountInfo>().Copy(info);
        }

        gameObject.SetActive(true);
        meetingData =new MeetingData(_meetingData,SysyemManager.SM.UserEmail);
        meetingName.text = _meetingData.meetingName;
        roomSelect.text = _meetingData.roomName;
        maxMemberAmount.text = _meetingData.maxMemberAmount.ToString();
        meetingInfo.text = _meetingData.meetingInfo;
        timeStart.text = _meetingData.meetingTime.startTime.Year.ToString() + "年" + _meetingData.meetingTime.startTime.Month.ToString() + "月" + _meetingData.meetingTime.startTime.Day.ToString() + "日";
        timeStart.text += _meetingData.meetingTime.startTime.Hour.ToString() + "點";
        timeStart.text += _meetingData.meetingTime.startTime.Minute.ToString() + "分";
        timeEnd.text = _meetingData.meetingTime.endTime.Year.ToString() + "年" + _meetingData.meetingTime.endTime.Month.ToString() + "月" + _meetingData.meetingTime.endTime.Day.ToString() + "日";
        timeEnd.text += _meetingData.meetingTime.endTime.Hour.ToString() + "點";
        timeEnd.text += _meetingData.meetingTime.endTime.Minute.ToString() + "分";
    }

    public void Leave()
    {
        //從會議離開
        RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime).Leave(SysyemManager.SM.UserEmail);
        string userID = "";
        for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
        {
            if (userInfoLoader.GetMail(i) == SysyemManager.SM.UserEmail)
            {
                userID = userInfoLoader.GetID(i);
                break;
            }
        }
        for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
        {
            if (meetingInfoLoader.GetMeetingname(i) == meetingName.text && meetingInfoLoader.GetRoomname(i) == roomSelect.text && meetingInfoLoader.GetMeetinginfo(i) == meetingInfo.text && meetingInfoLoader.GetMaxmemberamount(i) == int.Parse(maxMemberAmount.text))
            {
                string memberID = meetingInfoLoader.GetMember(i).Remove(meetingInfoLoader.GetMember(i).IndexOf(userID), 2);//從member中移除此成員
                updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), memberID, meetingInfoLoader.GetInvite(i), meetingInfoLoader.GetApply(i));
                break;
            }
        }
    }
}
