﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Search : MonoBehaviour
{
    private string meetingName="";
    private string memberName="";
    private string roomName="";
    private bool relatiedSelf=true;
    private bool sreachTime = false;
    private MeetingTime meetingTime;

    private bool setTimeStartOrEnd;
    [SerializeField] private Text timeStart;
    [SerializeField] private InputField timeStart_hour;
    [SerializeField] private InputField timeStart_minute;
    [SerializeField] private Text timeEnd;
    [SerializeField] private InputField timeEnd_hour;
    [SerializeField] private InputField timeEnd_minute;
    [SerializeField] private DatePicker datePicker;
    [SerializeField] private GameObject sreachResult;
    [SerializeField] private GameObject sreachSlot;
    void Start()
    {
        meetingName = "";
        memberName = "";
        roomName = "";
        relatiedSelf = true;
        meetingTime = new MeetingTime(new DateTime(DateTime.Now.Day));
        timeStart.text = meetingTime.startTime.Year.ToString() + "年" + meetingTime.startTime.Month.ToString() + "月" + meetingTime.startTime.Day.ToString() + "日";
        timeEnd.text = meetingTime.endTime.Year.ToString() + "年" + meetingTime.endTime.Month.ToString() + "月" + meetingTime.endTime.Day.ToString() + "日";
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void SearchMeeting()
    {
        for (int i=0;i< sreachResult.transform.childCount;i++)
        {
            Destroy(sreachResult.transform.GetChild(i).gameObject);
        }
        SearchRoomName();
    }
    private void SearchRoomName()
    {
        if (roomName == "")
        {
            for (int i=0;i< RoomManager.instance.rooms.Count;i++)
            {
                SearchMeetingName(RoomManager.instance.rooms[i].GetComponent<MeetingRoom>());
            }
        }
        else
            SearchMeetingName(RoomManager.instance.FindRoomWithName(roomName));
    }
    private void SearchMeetingName(MeetingRoom _meetingRoom)
    {
        if (_meetingRoom == null)
            return;
        if (meetingName == "")
        {
            for (int i = 0; i < _meetingRoom.meetingList.Count; i++)
            {
                SearchMember(_meetingRoom.meetingList[i]);
            }
        }
        else
        {
            for (int i = 0; i < _meetingRoom.meetingList.Count; i++)
            {
                if (meetingName == _meetingRoom.meetingList[i].meetingName)
                {
                    SearchMember(_meetingRoom.meetingList[i]);
                }
            }
        }
    }
    private void SearchMember(MeetingData _meetingData)
    {
        //找成員
        if (relatiedSelf)
        {
            if (_meetingData.SearchMember(SysyemManager.SM.UserEmail))
            {
                SearchTime(_meetingData);
            }
        }
        else
        {
            SearchTime(_meetingData);
        }
    }
    private void SearchTime(MeetingData _meetingData)
    {
        if (sreachTime)
        {
            if ((meetingTime.startTime <= _meetingData.meetingTime.startTime&& _meetingData.meetingTime.startTime<= meetingTime.endTime)||
               (meetingTime.endTime <= _meetingData.meetingTime.endTime && _meetingData.meetingTime.startTime <= meetingTime.endTime))
            {
                SearchSlot searchSlot = Instantiate(sreachSlot, sreachResult.transform).GetComponent<SearchSlot>();
                searchSlot.SetMeetingData(_meetingData);
                searchSlot.slotText.text = _meetingData.meetingName;
            }
        }
        else
        {
            SearchSlot searchSlot = Instantiate(sreachSlot, sreachResult.transform).GetComponent<SearchSlot>();
            searchSlot.SetMeetingData(_meetingData);
            searchSlot.slotText.text = _meetingData.meetingName;
        }
    }
    public void SetTimeDay(bool _bool)
    {
        datePicker.gameObject.SetActive(true);
        setTimeStartOrEnd = _bool;
    }
    public void ConfirmSetTime()
    {
        if (setTimeStartOrEnd)
        {
            timeStart.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingTime.startTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingTime.startTime.Hour, meetingTime.startTime.Minute, 0);
        }
        else
        {
            timeEnd.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingTime.endTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingTime.endTime.Hour, meetingTime.endTime.Minute, 0);
        }
        datePicker.gameObject.SetActive(false);
    }
    public void SetStartTime_hour(string _text)
    {
        int m = meetingTime.startTime.Hour;
        try
        {
            m = int.Parse(_text);
            if (m > 23 || m < 0)
            {
                m = meetingTime.startTime.Hour;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogError("容納數量需輸入數字");
        }
        meetingTime.startTime = new System.DateTime(meetingTime.startTime.Year, meetingTime.startTime.Month, meetingTime.startTime.Day, m, meetingTime.startTime.Minute, 0);
        timeStart_hour.text = m.ToString();
    }
    public void SetEndTime_hour(string _text)
    {
        int m = meetingTime.endTime.Hour;
        try
        {
            m = int.Parse(_text);
            if (m > 23 || m < 0)
            {
                m = meetingTime.endTime.Hour;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingTime.endTime = new System.DateTime(meetingTime.endTime.Year, meetingTime.endTime.Month, meetingTime.endTime.Day, m, meetingTime.endTime.Minute, 0);
        timeEnd_hour.text = m.ToString();
    }
    public void SetEndTime_minute(string _text)
    {
        int m = meetingTime.endTime.Minute;
        try
        {
            m = int.Parse(_text);
            if (m > 59 || m < 0)
            {
                m = meetingTime.endTime.Minute;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingTime.endTime = new System.DateTime(meetingTime.endTime.Year, meetingTime.endTime.Month, meetingTime.endTime.Day, meetingTime.endTime.Hour, m, 0);
        timeEnd_minute.text = m.ToString();
    }
    public void SetStartTime_minute(string _text)
    {
        int m = meetingTime.startTime.Minute;
        try
        {
            m = int.Parse(_text);
            if (m > 59 || m < 0)
            {
                m = meetingTime.startTime.Minute;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingTime.startTime = new System.DateTime(meetingTime.startTime.Year, meetingTime.startTime.Month, meetingTime.startTime.Day, meetingTime.startTime.Hour, m, 0);
        timeStart_minute.text = m.ToString();
    }
    public void SetMeetingName(string _text)
    {
        meetingName = _text;
    }
    public void SetMemberName(string _text)
    {
        memberName = _text;
    }
    public void SetRoomName(string _text)
    {
        roomName = _text;
    }
    public void SetRelatedSelf(bool _bool)
    {
        relatiedSelf = _bool;
    }
    public void SetSreachTime(bool _bool)
    {
        sreachTime = _bool;
    }
}