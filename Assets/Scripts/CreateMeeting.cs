﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateMeeting : MonoBehaviour
{
    [HideInInspector]
    [SerializeField] private MeetingReserve meetingReserve;

    [SerializeField] private InputField meetingName_input;
    [SerializeField] private Text roomSelect;
    [SerializeField] private InputField maxMemberAmount_input;
    [SerializeField] private InputField meetingInfo_text;
    [SerializeField] private Text timeStart;
    [SerializeField] private InputField timeStart_hour;
    [SerializeField] private InputField timeStart_minute;
    [SerializeField] private Text timeEnd;
    [SerializeField] private InputField timeEnd_hour;
    [SerializeField] private InputField timeEnd_minute;
    [SerializeField] private DatePicker datePicker;
    [SerializeField] private Transform memberSlot;
    [SerializeField] private GameObject UserInfo;

    private MeetingData meetingData = new MeetingData("", "18 18 18 ", "", new MeetingTime(DateTime.Now), 20, "");
    //private int roomSelect_DropdownIndex = 0;
    private bool enableButton_Invite = false;
    private bool setTimeStartOrEnd = true;
    private int chooseRoom;

    [SerializeField] private InsertMeeting insertMeeting;
    [SerializeField] private DataLoader dataLoader;

    public void RefreshPanel(DateTime _dateTime)
    {
        enableButton_Invite = false;
        SetAccountType();
        for (int i = memberSlot.childCount - 1; i >= 0; i--)
            Destroy(memberSlot.GetChild(i).gameObject);

        gameObject.SetActive(true);

        meetingData.host = SysyemManager.SM.UserEmail;
        meetingData.meetingName = "";
        meetingName_input.text = meetingData.meetingName;
        meetingData.maxMemberAmount = 20;
        maxMemberAmount_input.text = meetingData.maxMemberAmount.ToString();
        meetingData.meetingInfo = "";
        meetingInfo_text.text = meetingData.meetingInfo;
        meetingData.invite.Clear();

        //roomSelect_DropdownIndex = 0;
        //ResetRoomSelect_Dropdown();

        meetingData.meetingTime.startTime = _dateTime;
        meetingData.meetingTime.endTime = _dateTime.AddHours(2);

        timeStart.text = meetingData.meetingTime.startTime.Year.ToString() + "年" + meetingData.meetingTime.startTime.Month.ToString() + "月" + meetingData.meetingTime.startTime.Day.ToString() + "日";
        timeStart_hour.text = meetingData.meetingTime.startTime.Hour.ToString();
        timeStart_minute.text = meetingData.meetingTime.startTime.Minute.ToString();
        timeEnd.text = meetingData.meetingTime.endTime.Year.ToString() + "年" + meetingData.meetingTime.endTime.Month.ToString() + "月" + meetingData.meetingTime.endTime.Day.ToString() + "日";
        timeEnd_hour.text = meetingData.meetingTime.endTime.Hour.ToString();
        timeEnd_minute.text = meetingData.meetingTime.endTime.Minute.ToString();
    }
    public void SetRoomSelectName(int _input)
    {
        chooseRoom = _input;
        roomSelect.text = RoomManager.instance.rooms[_input].GetComponent<MeetingRoom>().roomName;
        meetingData.roomName = roomSelect.text;
    }
    public void SetMeetingName(string _text)
    {
        meetingData.meetingName = _text;
    }
    //public void SetRoomSelect(int _index)
    //{
    //    meetingData.roomName = roomSelect_Dropdown.options[_index].text;
    //}
    //private void ResetRoomSelect_Dropdown()
    //{
    //roomSelect_Dropdown.ClearOptions();

    //List<string> Options = new List<string>();
    //foreach (var room in RoomManager.instance.rooms)
    //{           
    //    string Option = room.GetComponent<MeetingRoom>().roomName;
    //    Options.Add(Option);
    //}
    //Debug.LogError("還沒加房間名dropdown");

    //roomSelect_Dropdown.AddOptions(Options);
    //roomSelect_Dropdown.value = roomSelect_DropdownIndex;
    //roomSelect_Dropdown.RefreshShownValue();
    //}
    public void SetMemberAmount(string _text)
    {
        int m = meetingData.maxMemberAmount;
        try
        {
            m = int.Parse(_text);
        }
        catch (System.FormatException)
        {
            Debug.LogError("容納數量需輸入數字");
        }
        meetingData.maxMemberAmount = m;
        maxMemberAmount_input.text = m.ToString();
    }
    public void SetMeetingInfo(string _text)
    {
        meetingData.meetingInfo = _text;
    }
    public void SetTimeDay(bool _bool)
    {
        datePicker.gameObject.SetActive(true);
        setTimeStartOrEnd = _bool;
    }
    public void ConfirmSetTime()
    {
        if (setTimeStartOrEnd)
        {
            timeStart.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingData.meetingTime.startTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingData.meetingTime.startTime.Hour, meetingData.meetingTime.startTime.Minute, 0);
        }
        else
        {
            timeEnd.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingData.meetingTime.endTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingData.meetingTime.endTime.Hour, meetingData.meetingTime.endTime.Minute, 0);
        }
        datePicker.gameObject.SetActive(false);
    }
    public void SetStartTime_hour(string _text)
    {
        int m = meetingData.meetingTime.startTime.Hour;
        try
        {
            m = int.Parse(_text);
            if (m > 23 || m < 0)
            {
                m = meetingData.meetingTime.startTime.Hour;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogError("容納數量需輸入數字");
        }
        meetingData.meetingTime.startTime = new System.DateTime(meetingData.meetingTime.startTime.Year, meetingData.meetingTime.startTime.Month, meetingData.meetingTime.startTime.Day, m, meetingData.meetingTime.startTime.Minute, 0);
        timeStart_hour.text = m.ToString();
    }
    public void SetEndTime_hour(string _text)
    {
        int m = meetingData.meetingTime.endTime.Hour;
        try
        {
            m = int.Parse(_text);
            if (m > 23 || m < 0)
            {
                m = meetingData.meetingTime.endTime.Hour;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingData.meetingTime.endTime = new System.DateTime(meetingData.meetingTime.endTime.Year, meetingData.meetingTime.endTime.Month, meetingData.meetingTime.endTime.Day, m, meetingData.meetingTime.endTime.Minute, 0);
        timeEnd_hour.text = m.ToString();
    }
    public void SetEndTime_minute(string _text)
    {
        int m = meetingData.meetingTime.endTime.Minute;
        try
        {
            m = int.Parse(_text);
            if (m > 59 || m < 0)
            {
                m = meetingData.meetingTime.endTime.Minute;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingData.meetingTime.endTime = new System.DateTime(meetingData.meetingTime.endTime.Year, meetingData.meetingTime.endTime.Month, meetingData.meetingTime.endTime.Day, meetingData.meetingTime.endTime.Hour, m, 0);
        timeEnd_minute.text = m.ToString();
    }
    public void SetStartTime_minute(string _text)
    {
        int m = meetingData.meetingTime.startTime.Minute;
        try
        {
            m = int.Parse(_text);
            if (m > 59 || m < 0)
            {
                m = meetingData.meetingTime.startTime.Minute;
                Debug.LogWarning("輸入超過上下限!");
            }
        }
        catch (System.FormatException)
        {
            Debug.LogWarning("容納數量需輸入數字");
        }
        meetingData.meetingTime.startTime = new System.DateTime(meetingData.meetingTime.startTime.Year, meetingData.meetingTime.startTime.Month, meetingData.meetingTime.startTime.Day, meetingData.meetingTime.startTime.Hour, m, 0);
        timeStart_minute.text = m.ToString();
    }
    public void SetAccountType()
    {
        AccountManager.instance.accountType = AccountType.CreateMeeting;
    }
    public void Reserve()
    {
        if (RoomManager.instance.rooms[chooseRoom].GetComponent<MeetingRoom>().AddMeetingData(new MeetingData(meetingData, SysyemManager.SM.UserEmail)))
        {
            EmailSender.IE_SendEmail(meetingData.invite,meetingData.roomName,meetingData.meetingName,meetingData.meetingInfo);
            //RoomManager.instance.SaveRoomData(RoomManager.instance.rooms[chooseRoom].GetComponent<MeetingRoom>());
            //參加的成員ID
            string memberID = "", inviteMemberID = "", applyMemberID = "";
            for (int i = 0; i < meetingData.member.Count; i++)
            {
                for (int j = 0; j < dataLoader.participators.Length - 1; j++)
                {
                    if (dataLoader.GetMail(j) == meetingData.member[i])
                    {
                        memberID += dataLoader.GetID(j);
                        memberID += ",";
                    }
                }
            }
            //邀請的成員ID
            Debug.Log("meetingData.invite.Count = " + meetingData.invite.Count);
            for (int i = 0; i < meetingData.invite.Count; i++)
            {
                for (int j = 0; j < dataLoader.participators.Length - 1; j++)
                {
                    if (dataLoader.GetMail(j) == meetingData.invite[i])
                    {
                        inviteMemberID += dataLoader.GetID(j);
                        inviteMemberID += ",";
                    }
                }
            }
            //申請的成員ID
            Debug.Log("meetingData.require.Count = " + meetingData.require.Count);
            for (int i = 0; i < meetingData.require.Count; i++)
            {
                for (int j = 0; j < dataLoader.participators.Length - 1; j++)
                {
                    if (dataLoader.GetMail(j) == meetingData.require[i])
                    {
                        applyMemberID += dataLoader.GetID(j);
                        applyMemberID += ",";
                    }
                }
            }
            Debug.Log("insertMeeting.meetingnameIp.text = " + insertMeeting.meetingnameIp.text);
            Debug.Log("insertMeeting.meetingInfo.text = " + insertMeeting.meetingInfo.text);
            StartCoroutine(insertMeeting.Create_Meeting(insertMeeting.meetingnameIp.text, insertMeeting.roomText.text, insertMeeting.meetingInfo.text, insertMeeting.startTimeStr(), insertMeeting.endTimeStr(), int.Parse(insertMeeting.memberAmountIp.text), meetingData.host, memberID, inviteMemberID, applyMemberID, RoomManager.instance.rooms[chooseRoom].GetComponent<MeetingRoom>()));
            
            gameObject.SetActive(false);
        }
    }
    public void AddInvite(AccountInfo info)
    {
        if (meetingData.AddInvited(info.mailText.text))
            Instantiate(UserInfo, memberSlot).GetComponent<AccountInfo>().Copy(info);
    }
    public void RemoveInvite(AccountInfo info)
    {
        meetingData.RemoveInvited(info.mailText.text);
    }
    public void ClickToEditInvite()
    {
        enableButton_Invite = !enableButton_Invite;
        for (int i = 0; i < memberSlot.childCount; i++)
        {
            memberSlot.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_Invite);
        }
    }
}