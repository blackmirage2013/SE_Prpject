﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MessageSlot : MonoBehaviour
{
    public UpdateMeetings updateMeetings;
    public meetingInfoLoader meetingInfoLoader;
    public DataLoader userInfoLoader;

    public string roomName;
    public string userEmail;
    public DateTime startTime;
    public bool IsHost;
    public GameObject text;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Yes()
    {
        string userID = "";
        for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
        {
            if (userInfoLoader.GetMail(i) == userEmail)
            {
                userID = userInfoLoader.GetID(i);
                break;
            }
        }
        if (IsHost)
        {
            RoomManager.instance.FindRoomWithName(roomName).FindMeetingData(startTime).AcceptRequire(userEmail);            

            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetRoomname(i) == roomName && Convert.ToDateTime(meetingInfoLoader.GetStarttime(i)) == startTime)
                {
                    string memberID = meetingInfoLoader.GetMember(i) + userID + ",";//將此成員加入member中
                    string applyID = meetingInfoLoader.GetApply(i).Remove(meetingInfoLoader.GetApply(i).IndexOf(userID), 2);//從apply中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), memberID, meetingInfoLoader.GetInvite(i), applyID);
                    break;
                }
            }
        }
        else
        {
            RoomManager.instance.FindRoomWithName(roomName).FindMeetingData(startTime).AcceptInvited(userEmail);
            MeetingData meetingDataTemp = RoomManager.instance.FindRoomWithName(roomName).FindMeetingData(startTime);
            EmailSender.IE_Accept(meetingDataTemp.host, meetingDataTemp.roomName, meetingDataTemp.meetingName, meetingDataTemp.meetingInfo);

            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetRoomname(i) == roomName && Convert.ToDateTime(meetingInfoLoader.GetStarttime(i)) == startTime)
                {
                    string memberID = meetingInfoLoader.GetMember(i) + userID + ",";//將此成員加入member中
                    string inviteID = meetingInfoLoader.GetInvite(i).Remove(meetingInfoLoader.GetInvite(i).IndexOf(userID), 2);//從invite中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), memberID, inviteID, meetingInfoLoader.GetApply(i));
                    break;
                }
            }
        }
        Destroy(gameObject);
    }
    public void No()
    {
        string userID = "";
        for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
        {
            if (userInfoLoader.GetMail(i) == userEmail)
            {
                userID = userInfoLoader.GetID(i);
                break;
            }
        }
        if (IsHost)
        {
            RoomManager.instance.FindRoomWithName(roomName).FindMeetingData(startTime).RemoveRequire(userEmail);

            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetRoomname(i) == roomName && Convert.ToDateTime(meetingInfoLoader.GetStarttime(i)) == startTime)
                {
                    string applyID = meetingInfoLoader.GetApply(i).Remove(meetingInfoLoader.GetApply(i).IndexOf(userID), 2);//從apply中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), meetingInfoLoader.GetMember(i), meetingInfoLoader.GetInvite(i), applyID);
                    break;
                }
            }
        }
        else
        {
            RoomManager.instance.FindRoomWithName(roomName).FindMeetingData(startTime).RemoveInvited(userEmail);

            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetRoomname(i) == roomName && Convert.ToDateTime(meetingInfoLoader.GetStarttime(i)) == startTime)
                {
                    string inviteID = meetingInfoLoader.GetInvite(i).Remove(meetingInfoLoader.GetInvite(i).IndexOf(userID), 2);//從invite中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), meetingInfoLoader.GetMember(i), inviteID, meetingInfoLoader.GetApply(i));
                    break;
                }
            }
        }
        Destroy(gameObject);
    }
}
