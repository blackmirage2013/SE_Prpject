﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net.Mail;
using UnityEngine;

public static class EmailSender
{
    static private SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
    static private MailAddress from = new MailAddress("blackmirage2021@gmail.com","SEproject",System.Text.Encoding.UTF8);
    static private MailAddress to;
    static List<SenderClass> senderClasses = new List<SenderClass>();
    static bool sending=false;
    public static SenderDelay senderDelay;
    public static void Start()
    {
        client.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
    }

    private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        // Get the unique identifier for this asynchronous operation.
        string token = (string)e.UserState;

        if (e.Cancelled)
        {
            Debug.Log("Send canceled " + token);
        }
        if (e.Error != null)
        {
            Debug.Log("[ " + token + " ] " + " " + e.Error.ToString());
        }
        else
        {
            Debug.Log("Message sent.");
        }
        
        Debug.Log("senderClasses.Count="+senderClasses.Count);
        if (senderClasses.Count > 0)
        {
            senderDelay.Delay_sendEmail();
        }
        else
            sending = false;
        
    }
    public static void Delay_sendEmail()
    {
        SenderClass senderClassTemp = new SenderClass(senderClasses[0].to_MailAddress, senderClasses[0].Title, senderClasses[0].Body);
        senderClasses.RemoveAt(0);
        SendEmail(senderClassTemp.to_MailAddress, senderClassTemp.Title, senderClassTemp.Body);
    }
    public static void SendEmail(string to_MailAddress,string Title,string Body)
    {
        client.Credentials = new System.Net.NetworkCredential("blackmirage2021@gmail.com","SEproject");
        client.EnableSsl = true;

        MailAddress to = new MailAddress(to_MailAddress);

        MailMessage message = new MailMessage(from, to);

        message.Body = Body;
        message.BodyEncoding = System.Text.Encoding.UTF8;
        message.Subject = Title;
        message.SubjectEncoding = System.Text.Encoding.UTF8;
        // Set the method that is called back when the send operation ends.

        // The userState can be any object that allows your callback
        // method to identify this send operation.
        // For this example, the userToken is a string constant.
        string userState = "test message1";
        client.SendAsync(message, userState);
    }
    public static void IE_SendEmail(List<string> _user,string _roomName,string _meetingName, string _meetingInfo)
    {
        foreach (string temp in _user)
        {
            if (!sending)
            {
                sending = true;
                SendEmail(temp, "會議預約系統(會議邀請)", "會議室:" + _roomName + "\n\n會議名稱:" + _meetingName + "\n邀請您加入會議\n\n會議通知:" + _meetingInfo);
            }
            else
                senderClasses.Add(new SenderClass(temp, "會議預約系統(會議邀請)", "會議室:" + _roomName + "\n\n會議名稱:" + _meetingName + "\n邀請您加入會議\n\n會議通知:" + _meetingInfo));
        }
    }
    public static void IE_Accept(string _host, string _roomName, string _meetingName, string _meetingInfo)
    {
        if (!sending)
        {
            sending = true;
            SendEmail(_host, "會議預約系統(同意邀請)", "會議室:" + _roomName + "\n\n會議名稱:" + _meetingName + "\n\n受邀者:" + SysyemManager.SM.UserEmail + "\n同意加入會議");
        }
        else
            senderClasses.Add(new SenderClass(_host, "會議預約系統(同意邀請)", "會議室:" + _roomName + "\n\n會議名稱:" + _meetingName + "\n\n受邀者:" + SysyemManager.SM.UserEmail + "\n同意加入會議"));

    }
    public class SenderClass
    {
        public string to_MailAddress;
        public string Title;
        public string Body;
        public SenderClass(string _to_MailAddress, string _Title, string _Body)
        {
            to_MailAddress = _to_MailAddress;
            Title = _Title;
            Body = _Body;
        }
    }
}
