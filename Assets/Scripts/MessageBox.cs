﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageBox : MonoBehaviour
{
    [SerializeField] private GameObject Slots;
    [SerializeField] private GameObject videoFile;

    public UpdateMeetings updateMeetings;
    public meetingInfoLoader meetingInfoLoader;
    public DataLoader userInfoLoader;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        foreach (GameObject gameObject in RoomManager.instance.rooms)
        {
            foreach (MeetingData meetingData in gameObject.GetComponent<MeetingRoom>().meetingList)
            {
                if (meetingData.host == SysyemManager.SM.UserEmail)
                {
                    //將申請者寫進通知
                    foreach (string member in meetingData.require)
                    {
                        MessageSlot messageSlot = Instantiate(videoFile, Slots.transform).GetComponent<MessageSlot>();
                        messageSlot.roomName = meetingData.roomName;
                        messageSlot.userEmail = member;
                        messageSlot.startTime = meetingData.meetingTime.startTime;
                        messageSlot.IsHost = true;
                        messageSlot.text.transform.GetChild(0).GetComponent<Text>().text = AccountManager.instance.GetAccountInfo(member).nameText.text + "申請加入(" + meetingData.meetingName + ")會議";
                        messageSlot.text.transform.GetChild(1).GetComponent<Text>().text = member;
                        messageSlot.updateMeetings = updateMeetings;
                        messageSlot.meetingInfoLoader = meetingInfoLoader;
                        messageSlot.userInfoLoader = userInfoLoader;
                    }
                    continue;
                }

                foreach(string member in meetingData.invite)
                {
                    if (member == SysyemManager.SM.UserEmail)
                    {
                        //將邀請寫進通知
                        MessageSlot messageSlot = Instantiate(videoFile, Slots.transform).GetComponent<MessageSlot>();
                        messageSlot.roomName = meetingData.roomName;
                        messageSlot.userEmail = member;
                        messageSlot.startTime = meetingData.meetingTime.startTime;
                        messageSlot.IsHost = false;
                        messageSlot.text.transform.GetChild(0).GetComponent<Text>().text =AccountManager.instance.GetAccountInfo(meetingData.host).nameText.text+ "邀請加入(" + meetingData.meetingName + ")會議";
                        messageSlot.text.transform.GetChild(1).GetComponent<Text>().text = meetingData.host;
                        messageSlot.updateMeetings = updateMeetings;
                        messageSlot.meetingInfoLoader = meetingInfoLoader;
                        messageSlot.userInfoLoader = userInfoLoader;
                        break;
                    }
                }
            }
        }
    }
    private void OnDisable()
    {
        for (int i=0;i<Slots.transform.childCount;i++)
        {
            Destroy(Slots.transform.GetChild(i).gameObject);
        }
    }
}
