﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataLoader : MonoBehaviour
{
    public string[] participators = { };

    IEnumerator Start()
    {
        WWW participatorsinfo = new WWW("https://meetingroommanage.000webhostapp.com/participatorsinfo.php");
        yield return participatorsinfo;
        string participatorsinfoString = participatorsinfo.text;
        print(participatorsinfoString);
        //Debug.Log("length = " + participators.Length);
        participators = participatorsinfoString.Split(';');
        //for(int i = 0; i < participators.Length - 1; i++)
        //{
        //    print(GetDataValue(participators[i], "ID:"));
        //    print(GetDataValue(participators[i], "name:"));
        //    print(GetDataValue(participators[i], "email:"));
        //    print(GetDataValue(participators[i], "password:"));
        //}
        AccountManager.instance.UpdateAccountList();
        Invoke("Update_invoke", 1f);
    }
    void Update_invoke()
    {
        StartCoroutine(UpdateDataBase());
        Invoke("Update_invoke", 1f);
    }

    public IEnumerator UpdateDataBase()
    {
        WWW participatorsinfo = new WWW("https://meetingroommanage.000webhostapp.com/participatorsinfo.php");
        yield return participatorsinfo;
        string participatorsinfoString = participatorsinfo.text;
        print(participatorsinfoString);
        //Debug.Log("length = " + participators.Length);
        participators = participatorsinfoString.Split(';');
        //for(int i = 0; i < participators.Length - 1; i++)
        //{
        //    print(GetDataValue(participators[i], "ID:"));
        //    print(GetDataValue(participators[i], "name:"));
        //    print(GetDataValue(participators[i], "email:"));
        //    print(GetDataValue(participators[i], "password:"));
        //}
        AccountManager.instance.UpdateAccountList();
    }
    string GetDataValue(string data, string index)
    {
        if (data == "") return null;
        string value = data.Substring(data.IndexOf(index)+index.Length);
        if(value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }

    public string GetID(int index)
    {
        return GetDataValue(participators[index], "ID:");
    }

    public string GetMail(int index)
    {
        return GetDataValue(participators[index], "email:");
    }

    public string GetName(int index)
    {
        return GetDataValue(participators[index], "name:");
    }

    public string GetPwd(int index)
    {
        return GetDataValue(participators[index], "password:");
    }
}
