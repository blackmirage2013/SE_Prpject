﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class DataInserter : MonoBehaviour
{
    string CreateUserURL = "https://meetingroommanage.000webhostapp.com/insertUser.php";

    public InputField usernameIp;
    public InputField passwordIp;
    public InputField check_passwordIp;
    public InputField emailIp;
    public InputField checkcodeIp;

    public Button m_confirmBtn;
    public Button m_sendBtn;

    public GameObject registerMenu;
    
    private int checkCode;

    // Start is called before the first frame update
    void Start()
    {
        m_sendBtn.onClick.AddListener(() =>
        {
            checkCode = Random.Range(1000, 9999);
            EmailSender.SendEmail(emailIp.text, "Conference Room Booking CheckCode", checkCode.ToString());
        });
        m_confirmBtn.onClick.AddListener(() =>
        {
            //判斷是否都有輸入
            if (usernameIp.text != "" && passwordIp.text != "" && emailIp.text != "" && check_passwordIp.text != "")
            {
                //判斷輸入的密碼及再次輸入的是否一致
                //判斷驗證碼是否正確
                if ((passwordIp.text == check_passwordIp.text) && (checkcodeIp.text == checkCode.ToString()))
                {
                    StartCoroutine(CreateUser(usernameIp.text, passwordIp.text, emailIp.text));
                    //CreateUser(usernameIp.text, passwordIp.text, emailIp.text);
                    ClearIpText(usernameIp);
                    ClearIpText(passwordIp);
                    ClearIpText(check_passwordIp);
                    ClearIpText(emailIp);
                    ClearIpText(checkcodeIp);
                    registerMenu.SetActive(false);
                }
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
        //if (Input.GetKeyDown(KeyCode.Space)) CreateUser(m_usernameIp, inputPassword, inputEmail);
    }

    static public void ClearIpText(InputField inputfield)
    {
        inputfield.text = "";
    }

   IEnumerator CreateUser(string username, string password, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);
        form.AddField("emailPost", email);

        using (UnityWebRequest www = UnityWebRequest.Post(CreateUserURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
    /*public void CreateUser(string username, string password, string email)
    {
        WWWForm form = new WWWForm();
        form.AddField("usernamePost", username);
        form.AddField("passwordPost", password);
        form.AddField("emailPost", email);

        WWW www = new WWW(CreateUserURL, form);
        Debug.Log(www.text);
    }*/
}
