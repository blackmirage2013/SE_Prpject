﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour
{
    /*public string inputUserName;
    public string inputPassword;*/

    string LoginURL = "https://meetingroommanage.000webhostapp.com/Login.php";

    public InputField emailIp;
    public InputField passwordIp;
    public Toggle saveEmail;
    public Button m_loginBtn;

    // Start is called before the first frame update
    void Start()
    {
        string f = Application.streamingAssetsPath + "/Email.json";
        if (System.IO.File.Exists(f))
        {
            
            var json = System.IO.File.ReadAllText(f);
            //反序列化成Caption_Data物件
            var newData = JsonUtility.FromJson < Email > (json);
            Email e = newData;
            emailIp.text = e.email;
        }
        m_loginBtn.onClick.AddListener(() =>
        {
            StartCoroutine(LoginToDB(emailIp.text, passwordIp.text));
        });
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.L)) StartCoroutine(LoginToDB(inputUserName, inputPassword));
    }

    IEnumerator LoginToDB(string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("emailPost", email);
        form.AddField("passwordPost", password);

        WWW www = new WWW(LoginURL, form);
        yield return www;
        Debug.Log("wwwText = " + www.text);
        // error
        if(www.text != "user not found" && www.text != "password incorrect")
        {
            string lastChar = www.text.Substring(www.text.Length - 1, 1);
            string username = www.text.Remove(www.text.LastIndexOf("1"), 1);
            Debug.Log("username = " + username);
            //成功登入，切換場景
            if (lastChar == "1")
            {
                SysyemManager.SM.OnLoginSuccess(username, emailIp.text, saveEmail.isOn);
            }
        }
    }
}
