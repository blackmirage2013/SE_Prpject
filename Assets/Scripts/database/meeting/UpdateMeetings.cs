﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UpdateMeetings : MonoBehaviour
{
    string UpdateMeetingsURL = "https://meetingroommanage.000webhostapp.com/updateMeeting.php";

    public void UseStartcoroUpdateMeeting(int ID, string meetingname, string roomname, string meetinginfo, string startTime, string endTime, int memberamount, string hostMail, string memberID, string inviteID, string applyID)
    {
        StartCoroutine(updateMeeting(ID, meetingname, roomname, meetinginfo, startTime, endTime, memberamount, hostMail, memberID, inviteID, applyID));
    }
    public IEnumerator updateMeeting(int ID, string meetingname, string roomname, string meetinginfo, string startTime, string endTime, int memberamount, string hostMail, string memberID, string inviteID, string applyID)
    {
        WWWForm form = new WWWForm();
        form.AddField("idPost", ID);
        form.AddField("meetingnamePost", meetingname);
        form.AddField("roomnamePost", roomname);
        form.AddField("meetinginfoPost", meetinginfo);
        form.AddField("starttimePost", startTime);
        form.AddField("endtimePost", endTime);
        form.AddField("memberamountPost", memberamount);
        form.AddField("hostmailPost", hostMail);
        form.AddField("memberidPost", memberID);
        form.AddField("inviteidPost", inviteID);
        form.AddField("applyidPost", applyID);

        using (UnityWebRequest www = UnityWebRequest.Post(UpdateMeetingsURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
        Debug.Log("[完成會議更新]");
    }
}
