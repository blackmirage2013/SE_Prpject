﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeleteMeeting : MonoBehaviour
{

    string DeleteMeetingURL = "https://meetingroommanage.000webhostapp.com/deleteMeeting.php";

    public IEnumerator deleteMeeting(int ID)
    {
        WWWForm form = new WWWForm();
        form.AddField("idPost", ID);

        using (UnityWebRequest www = UnityWebRequest.Post(DeleteMeetingURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
}
