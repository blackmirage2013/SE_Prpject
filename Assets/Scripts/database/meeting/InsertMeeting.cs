﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class InsertMeeting : MonoBehaviour
{
    string InsertMeetingURL = "https://meetingroommanage.000webhostapp.com/insertMeeting.php";

    public InputField meetingnameIp;
    public Text roomText;
    public InputField meetingInfo;
    public Text startDateBtnText;
    public InputField startTimeIp_hr;
    public InputField startTimeIp_min;
    public Text endDateBtnText;
    public InputField endTimeIp_hr;
    public InputField endTimeIp_min;
    public InputField memberAmountIp;
    public GameObject CreateMeetingWindow;

    public int memberAmountINT;
    //public string startDatetimeStr, endDatetimeStr;
    //private string hostEmail;
    //public GameObject registerMemberList;

    private GameObject[] m_userEmail;
    private Text userMail;
    public string inviteMemberID = "";//邀請的成員
    public string memberID = "";//參加的成員
    public string applyMemberID = "";//申請的成員

    public Button m_saveBtn;
    public DataLoader dataLoader;
    //Login loginUser;
    public MeetingData meetingData;

    public UpdateRoom update_room;
    public RoomInfoLoader roomInfoLoader;
    public meetingInfoLoader meetingInfo_loader;
    public RoomManager roomManager;
    // Start is called before the first frame update
    void Start()
    {
        //meetingData = GameObject.Find("Main Camera").GetComponent<MeetingData>(); 這不是comp
        //loginUser = GameObject.Find("Main Camera").GetComponent<Login>();
        //確認新增meeting
        //m_saveBtn.onClick.AddListener(() =>
        //{
        //    if (meetingnameIp.text != "" && startTimeIp_hr.text != "" && startTimeIp_min.text != "" && endTimeIp_hr.text != "" && endTimeIp_min.text != "" && memberAmountIp.text != "" && startDateBtnText.text != "Button" && endDateBtnText.text != "Button")
        //    {
        //        string startDatetimeStr, endDatetimeStr;
        //        //開始時間
        //        startDatetimeStr = startDateBtnText.text.Substring(0, 4);
        //        startDatetimeStr += "-";
        //        //加上月份
        //        if (startDateBtnText.text[6] == '月') //1~9月
        //        {
        //            startDatetimeStr += '0';
        //            startDatetimeStr += startDateBtnText.text[5];
        //            startDatetimeStr += "-";
        //            //加上日期
        //            if (startDateBtnText.text[8] == '日')//1~9號
        //            {
        //                startDatetimeStr += '0';
        //                startDatetimeStr += startDateBtnText.text[7];
        //                startDatetimeStr += " ";
        //            }
        //            else//10號之後
        //            {
        //                startDatetimeStr += startDateBtnText.text.Substring(7, 2);
        //                startDatetimeStr += " ";
        //            }
        //        }
        //        else//10月、11月、12月
        //        {
        //            startDatetimeStr += startDateBtnText.text.Substring(5, 2);
        //            startDatetimeStr += "-";
        //            //加上日期
        //            if (startDateBtnText.text[9] == '日')//1~9號
        //            {
        //                startDatetimeStr += '0';
        //                startDatetimeStr += startDateBtnText.text[8];
        //                startDatetimeStr += " ";
        //            }
        //            else//10號之後
        //            {
        //                startDatetimeStr += startDateBtnText.text.Substring(8, 2);
        //                startDatetimeStr += " ";
        //            }
        //        }
        //        //日期後加上時間
        //        if (startTimeIp_hr.text.Length == 1)//0~9點
        //        {
        //            startDatetimeStr += '0';
        //            startDatetimeStr += startTimeIp_hr.text;
        //        }
        //        else//10~24點
        //        {
        //            startDatetimeStr += startTimeIp_hr.text;
        //        }
        //        startDatetimeStr += ":";
        //        if (startTimeIp_min.text.Length == 1)//0~9分
        //        {
        //            startDatetimeStr += '0';
        //            startDatetimeStr += startTimeIp_min.text;
        //        }
        //        else//10~59分
        //        {
        //            startDatetimeStr += startTimeIp_min.text;
        //        }
        //        startDatetimeStr += ":00";
        //        //結束時間
        //        endDatetimeStr = endDateBtnText.text.Substring(0, 4);
        //        endDatetimeStr += "-";
        //        //加上月份
        //        if (startDateBtnText.text[6] == '月') //1~9月
        //        {
        //            endDatetimeStr += '0';
        //            endDatetimeStr += endDateBtnText.text[5];
        //            endDatetimeStr += "-";
        //            //加上日期
        //            if (startDateBtnText.text[8] == '日')//1~9號
        //            {
        //                endDatetimeStr += '0';
        //                endDatetimeStr += endDateBtnText.text[7];
        //                endDatetimeStr += " ";
        //            }
        //            else//10號之後
        //            {
        //                endDatetimeStr += endDateBtnText.text.Substring(7, 2);
        //                endDatetimeStr += " ";
        //            }
        //        }
        //        else//10月、11月、12月
        //        {
        //            endDatetimeStr += endDateBtnText.text.Substring(5, 2);
        //            endDatetimeStr += "-";
        //            //加上日期
        //            if (startDateBtnText.text[9] == '日')//1~9號
        //            {
        //                endDatetimeStr += '0';
        //                endDatetimeStr += endDateBtnText.text[8];
        //                endDatetimeStr += " ";
        //            }
        //            else//10號之後
        //            {
        //                endDatetimeStr += endDateBtnText.text.Substring(8, 2);
        //                endDatetimeStr += " ";
        //            }
        //        }
        //        //日期後加上時間
        //        if (endTimeIp_hr.text.Length == 1)//0~9點
        //        {
        //            endDatetimeStr += '0';
        //            endDatetimeStr += endTimeIp_hr.text;
        //        }
        //        else//10~24點
        //        {
        //            endDatetimeStr += endTimeIp_hr.text;
        //        }
        //        endDatetimeStr += ":";
        //        if (startTimeIp_min.text.Length == 1)//0~9分
        //        {
        //            endDatetimeStr += '0';
        //            endDatetimeStr += endTimeIp_min.text;
        //        }
        //        else//10~59分
        //        {
        //            endDatetimeStr += endTimeIp_min.text;
        //        }
        //        endDatetimeStr += ":00";

        //        //memberAmountINT = int.Parse(memberAmountIp.text);

        //        /*registerMemberList.SetActive(false);
        //        m_userEmail = GameObject.FindGameObjectsWithTag("userEmail");
        //        for (int i = 0; i < m_userEmail.Length; i++)
        //        {
        //            userMail = m_userEmail[i].GetComponent<Text>();
        //            for (int k = 0; k < dataLoader.participators.Length; k++)
        //            {
        //                if (userMail.text == dataLoader.GetMail(k))
        //                {
        //                    inviteMemberID += dataLoader.GetID(k);
        //                    inviteMemberID += ",";
        //                    break;
        //                }
        //            }
        //        }*/
        //        //hostEmail = loginUser.emailIp.text;
        //        //參加的成員ID
        //        for (int i = 0; i < meetingData.member.Count; i++)
        //        {
        //            for (int j = 0; j < dataLoader.participators.Length - 1; j++)
        //            {
        //                if (dataLoader.GetMail(j) == meetingData.member[i])
        //                {
        //                    memberID += dataLoader.GetID(j);
        //                    memberID += ",";
        //                }
        //            }
        //        }
        //        //邀請的成員ID
        //        Debug.Log("meetingData.invite.Count = " + meetingData.invite.Count);
        //        for (int i = 0; i < meetingData.invite.Count; i++)
        //        {
        //            for (int j = 0; j < dataLoader.participators.Length - 1; j++)
        //            {
        //                if (dataLoader.GetMail(j) == meetingData.invite[i])
        //                {
        //                    inviteMemberID += dataLoader.GetID(j);
        //                    inviteMemberID += ",";
        //                }
        //            }
        //        }
        //        //申請的成員ID
        //        for (int i = 0; i < meetingData.require.Count; i++)
        //        {
        //            for (int j = 0; j < dataLoader.participators.Length - 1; j++)
        //            {
        //                if (dataLoader.GetMail(j) == meetingData.require[i])
        //                {
        //                    applyMemberID += dataLoader.GetID(j);
        //                    applyMemberID += ",";
        //                }
        //            }
        //        }
        //        //Debug.Log("meetingname = " + meetingnameIp.text);
        //        //Debug.Log("roomname = " + roomText.text);
        //        //Debug.Log("meetingInfo = " + meetingInfo.text);
        //        //Debug.Log("startDatetimeStr = " + startDatetimeStr);
        //        //Debug.Log("endDatetimeStr = " + endDatetimeStr);
        //        //Debug.Log("memberAmountINT = " + memberAmountINT);
        //        //Debug.Log("host = " + meetingData.host);
        //        //Debug.Log("memberID = " + memberID);
        //        //Debug.Log("inviteMemberID = " + inviteMemberID);
        //        //Debug.Log("applyMemberID = " + applyMemberID);
                
        //        //StartCoroutine(Create_Meeting(meetingnameIp.text, roomText.text, meetingInfo.text, startDatetimeStr, endDatetimeStr, memberAmountINT, meetingData.host, memberID, inviteMemberID, applyMemberID));

        //        //for (int i = 0; i < roomInfoLoader.rooms.Length - 1; i++)
        //        //{
        //        //    if (roomText.text == roomInfoLoader.GetRoomname(i))
        //        //    {
        //        //        Debug.Log("meetingInfo_loader.GetID = " + meetingInfo_loader.GetID(meetingInfo_loader.meetings.Length - 2));
        //        //        string tmp = roomInfoLoader.GetMeetings_id(i) + meetingInfo_loader.GetID(meetingInfo_loader.meetings.Length - 2) + ",";
        //        //        StartCoroutine(update_room.updateRoom(roomInfoLoader.GetID(i), roomInfoLoader.GetRoomname(i), roomInfoLoader.GetStarttime(i), roomInfoLoader.GetEndtime(i), roomInfoLoader.GetRoomstateINT(i), roomInfoLoader.GetMember_id(i), tmp));
        //        //        break;
        //        //    }
        //        //}
        //        //for (int i = 0; i < roomInfoLoader.rooms.Length - 1; i++)
        //        //{
        //        //    string tmp = "";
        //        //    for (int j = 0; j < meetingInfo_loader.meetings.Length - 1; j++)
        //        //    {
        //        //        if (meetingInfo_loader.GetRoomname(j) == roomInfoLoader.GetRoomname(i))
        //        //        {
        //        //            tmp += meetingInfo_loader.GetID(j);
        //        //            tmp += ",";
        //        //        }
        //        //    }
        //        //    StartCoroutine(update_room.updateRoom(roomInfoLoader.GetID(i), roomInfoLoader.GetRoomname(i), roomInfoLoader.GetStarttime(i), roomInfoLoader.GetEndtime(i), roomInfoLoader.GetRoomstateINT(i), roomInfoLoader.GetMember_id(i), tmp));
        //        //}

        //        CreateMeetingWindow.SetActive(false);
        //    }

        //    else
        //    {
        //        Debug.Log("哭阿梅!");
        //    }
                
        //});
    }

    public string startTimeStr()
    {
        string startDatetimeStr;
        //開始時間
        startDatetimeStr = startDateBtnText.text.Substring(0, 4);
        startDatetimeStr += "-";
        //加上月份
        if (startDateBtnText.text[6] == '月') //1~9月
        {
            startDatetimeStr += '0';
            startDatetimeStr += startDateBtnText.text[5];
            startDatetimeStr += "-";
            //加上日期
            if (startDateBtnText.text[8] == '日')//1~9號
            {
                startDatetimeStr += '0';
                startDatetimeStr += startDateBtnText.text[7];
                startDatetimeStr += " ";
            }
            else//10號之後
            {
                startDatetimeStr += startDateBtnText.text.Substring(7, 2);
                startDatetimeStr += " ";
            }
        }
        else//10月、11月、12月
        {
            startDatetimeStr += startDateBtnText.text.Substring(5, 2);
            startDatetimeStr += "-";
            //加上日期
            if (startDateBtnText.text[9] == '日')//1~9號
            {
                startDatetimeStr += '0';
                startDatetimeStr += startDateBtnText.text[8];
                startDatetimeStr += " ";
            }
            else//10號之後
            {
                startDatetimeStr += startDateBtnText.text.Substring(8, 2);
                startDatetimeStr += " ";
            }
        }
        //日期後加上時間
        if (startTimeIp_hr.text.Length == 1)//0~9點
        {
            startDatetimeStr += '0';
            startDatetimeStr += startTimeIp_hr.text;
        }
        else//10~24點
        {
            startDatetimeStr += startTimeIp_hr.text;
        }
        startDatetimeStr += ":";
        if (startTimeIp_min.text.Length == 1)//0~9分
        {
            startDatetimeStr += '0';
            startDatetimeStr += startTimeIp_min.text;
        }
        else//10~59分
        {
            startDatetimeStr += startTimeIp_min.text;
        }
        startDatetimeStr += ":00";

        return startDatetimeStr;
    }

    public string endTimeStr()
    {
        string endDatetimeStr;
        //結束時間
        endDatetimeStr = endDateBtnText.text.Substring(0, 4);
        endDatetimeStr += "-";
        //加上月份
        if (startDateBtnText.text[6] == '月') //1~9月
        {
            endDatetimeStr += '0';
            endDatetimeStr += endDateBtnText.text[5];
            endDatetimeStr += "-";
            //加上日期
            if (startDateBtnText.text[8] == '日')//1~9號
            {
                endDatetimeStr += '0';
                endDatetimeStr += endDateBtnText.text[7];
                endDatetimeStr += " ";
            }
            else//10號之後
            {
                endDatetimeStr += endDateBtnText.text.Substring(7, 2);
                endDatetimeStr += " ";
            }
        }
        else//10月、11月、12月
        {
            endDatetimeStr += endDateBtnText.text.Substring(5, 2);
            endDatetimeStr += "-";
            //加上日期
            if (startDateBtnText.text[9] == '日')//1~9號
            {
                endDatetimeStr += '0';
                endDatetimeStr += endDateBtnText.text[8];
                endDatetimeStr += " ";
            }
            else//10號之後
            {
                endDatetimeStr += endDateBtnText.text.Substring(8, 2);
                endDatetimeStr += " ";
            }
        }
        //日期後加上時間
        if (endTimeIp_hr.text.Length == 1)//0~9點
        {
            endDatetimeStr += '0';
            endDatetimeStr += endTimeIp_hr.text;
        }
        else//10~24點
        {
            endDatetimeStr += endTimeIp_hr.text;
        }
        endDatetimeStr += ":";
        if (startTimeIp_min.text.Length == 1)//0~9分
        {
            endDatetimeStr += '0';
            endDatetimeStr += endTimeIp_min.text;
        }
        else//10~59分
        {
            endDatetimeStr += endTimeIp_min.text;
        }
        endDatetimeStr += ":00";

        return endDatetimeStr;
    }

    public IEnumerator Create_Meeting(string meetingname, string roomname, string meetingInfo, string startTime, string endTime, int memberAmount, string hostEmail, string member, string invite, string apply, MeetingRoom data)
    {
        WWWForm form = new WWWForm();
        form.AddField("meetingnamePost", meetingname);
        form.AddField("roomnamePost", roomname);
        form.AddField("meetinginfoPost", meetingInfo);
        form.AddField("starttimePost", startTime);
        form.AddField("endtimePost", endTime);
        form.AddField("memberamountPost", memberAmount);
        form.AddField("hostemailPost", hostEmail);
        form.AddField("memberPost", member);
        form.AddField("invitePost", invite);
        form.AddField("applyPost", apply);
        
        using (UnityWebRequest www = UnityWebRequest.Post(InsertMeetingURL, form))
        {
            UnityWebRequestAsyncOperation a = www.SendWebRequest();
            roomManager.DelaySave(data);
            yield return a;
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
       
    }
}