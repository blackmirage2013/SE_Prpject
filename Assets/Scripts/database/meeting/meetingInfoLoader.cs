﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class meetingInfoLoader : MonoBehaviour
{
    public string[] meetings = { };

    IEnumerator Start()
    {
        WWW meetingsInfo = new WWW("https://meetingroommanage.000webhostapp.com/meetingInfo.php");
        yield return meetingsInfo;
        string meetingsinfoString = meetingsInfo.text;
        print(meetingsinfoString);
        meetings = meetingsinfoString.Split(';');
        Invoke("Update_invoke", 1f);
        //for (int i = 0; i < meetings.Length - 1; i++)
        //{
        //    print(GetDataValue(meetings[i], "ID:"));
        //    print(GetDataValue(meetings[i], "meetingname:"));
        //    print(GetDataValue(meetings[i], "roomname:"));
        //    print(GetDataValue(meetings[i], "meetinginfo:"));
        //    print(GetDataValue(meetings[i], "starttime:"));
        //    print(GetDataValue(meetings[i], "endtime:"));
        //    print(GetDataValue(meetings[i], "memberamount:"));
        //    print(GetDataValue(meetings[i], "host:"));
        //    print(GetDataValue(meetings[i], "member:"));
        //    print(GetDataValue(meetings[i], "invite:"));
        //    print(GetDataValue(meetings[i], "apply:"));
        //}
    }
    void Update_invoke()
    {
        StartCoroutine(UpdateDataBase());
        Invoke("Update_invoke", 1f);
    }

    public void UseStartCoroUpdate()
    {
        StartCoroutine(UpdateDataBase());
        Invoke("Update_invoke", 1f);
    }

    public IEnumerator UpdateDataBase()
    {
        WWW meetingsInfo = new WWW("https://meetingroommanage.000webhostapp.com/meetingInfo.php");
        yield return meetingsInfo;

        string meetingsinfoString = meetingsInfo.text;
        print(meetingsinfoString);
        meetings = meetingsinfoString.Split(';');


        //for (int i = 0; i < meetings.Length - 1; i++)
        //{
        //    print(GetDataValue(meetings[i], "ID:"));
        //    print(GetDataValue(meetings[i], "meetingname:"));
        //    print(GetDataValue(meetings[i], "roomname:"));
        //    print(GetDataValue(meetings[i], "meetinginfo:"));
        //    print(GetDataValue(meetings[i], "starttime:"));
        //    print(GetDataValue(meetings[i], "endtime:"));
        //    print(GetDataValue(meetings[i], "memberamount:"));
        //    print(GetDataValue(meetings[i], "host:"));
        //    print(GetDataValue(meetings[i], "member:"));
        //    print(GetDataValue(meetings[i], "invite:"));
        //    print(GetDataValue(meetings[i], "apply:"));
        //}
        //Debug.Log("[完成會議更新]");
    }

    string GetDataValue(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }

    public string GetID(int index)
    {
        return GetDataValue(meetings[index], "ID:");
    }

    public string GetMeetingname(int index)
    {
        return GetDataValue(meetings[index], "meetingname:");
    }

    public string GetRoomname(int index)
    {
        return GetDataValue(meetings[index], "roomname:");
    }

    public string GetMeetinginfo(int index)
    {
        return GetDataValue(meetings[index], "meetinginfo:");
    }

    public string GetStarttime(int index)
    {
        return GetDataValue(meetings[index], "starttime:");
    }

    public string GetEndtime(int index)
    {
        return GetDataValue(meetings[index], "endtime:");
    }

    public int GetMaxmemberamount(int index)
    {
        int tmp = int.Parse(GetDataValue(meetings[index], "memberamount:"));
        return tmp;
    }

    public string GetHost(int index)
    {
        return GetDataValue(meetings[index], "host:");
    }

    public string GetMember(int index)
    {
        return GetDataValue(meetings[index], "member:");
    }

    public string GetInvite(int index)
    {
        return GetDataValue(meetings[index], "invite:");
    }

    public string GetApply(int index)
    {
        return GetDataValue(meetings[index], "apply:");
    }
}
