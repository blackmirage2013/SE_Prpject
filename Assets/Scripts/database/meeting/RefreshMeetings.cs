﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class RefreshMeetings : MonoBehaviour
{
    public Button m_staffBtn;
    public UpdateRoom update_room;
    public RoomInfoLoader roomInfoLoader;
    public meetingInfoLoader meetingInfo_loader;
    void Start()
    {
        m_staffBtn.onClick.AddListener(() =>
        {
            for (int i = 0; i < roomInfoLoader.rooms.Length - 1; i++)
            {
                string tmp = "";
                for (int j = 0; j < meetingInfo_loader.meetings.Length - 1; j++)
                {
                    if (meetingInfo_loader.GetRoomname(j) == roomInfoLoader.GetRoomname(i))
                    {
                        tmp += meetingInfo_loader.GetID(j);
                        tmp += ",";
                    }
                }
                update_room.UseStartcoroUpdateRoom(roomInfoLoader.GetID(i), roomInfoLoader.GetRoomname(i), roomInfoLoader.GetStarttime(i), roomInfoLoader.GetEndtime(i), roomInfoLoader.GetRoomstateINT(i), roomInfoLoader.GetMember_id(i), tmp);
                Debug.Log("roomInfoLoader.GetID(i) = " + roomInfoLoader.GetID(i));
            }
        });
    }
}