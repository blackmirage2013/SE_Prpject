﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class CreateRoom : MonoBehaviour
{
    string CreateRoomURL = "https://meetingroommanage.000webhostapp.com/insertRoom.php";

    public InputField roomnameIp;
    public InputField startTimeIp_hr;
    public InputField startTimeIp_min;
    public InputField endTimeIp_hr;
    public InputField endTimeIp_min;
    public Toggle roomstateIp;
    public GameObject registerMemberList;

    private GameObject[] m_userEmail;
    private Text userMail;
    //private string appointmentMemberID = "";

    public Button m_saveBtn;

    DataLoader dataLoader;
    // Start is called before the first frame update
    void Start()
    {
        //dataLoader = GameObject.Find("Main Camera").GetComponent<DataLoader>();
        ////確認新增room
        //m_saveBtn.onClick.AddListener(() =>
        //{
        //    //判斷rommstate
        //    int roomstate;
        //    if (roomstateIp.isOn) { roomstate = 1; } 
        //    else { roomstate = 0; }
        //    //判斷是否都有輸入
        //    if (roomnameIp.text != "" && startTimeIp_hr.text != "" && startTimeIp_min.text != "" && endTimeIp_hr.text != "" && endTimeIp_min.text != "")
        //    {
        //        int startTimeHR, startTimeMIN, endTimeHR, endTimeMIN;
        //        startTimeHR = int.Parse(startTimeIp_hr.text);
        //        startTimeMIN = int.Parse(startTimeIp_min.text);
        //        endTimeHR = int.Parse(endTimeIp_hr.text);
        //        endTimeMIN = int.Parse(endTimeIp_min.text);
        //        int startTime, endTime;
        //        startTime = startTimeHR * 60 + startTimeMIN;
        //        endTime = endTimeHR * 60 + endTimeMIN;

        //        registerMemberList.SetActive(false);
        //        m_userEmail = GameObject.FindGameObjectsWithTag("userEmail");
        //        for (int i = 0; i < m_userEmail.Length; i++)
        //        {
        //            userMail = m_userEmail[i].GetComponent<Text>();
        //            for (int k = 0; k < dataLoader.participators.Length; k++)
        //            {
        //                if (userMail.text == dataLoader.GetMail(k))
        //                {
        //                    appointmentMemberID += dataLoader.GetID(k);
        //                    appointmentMemberID += ",";
        //                    //Debug.Log("memberID = " + appointmentMemberID);
        //                    break;
        //                }
        //            }
        //        }
        //        Debug.Log("appointment = " + appointmentMemberID);
        //        if (startTime!=endTime)
        //        {
        //            StartCoroutine(InsertRoom(roomnameIp.text, startTime, endTime, roomstate, appointmentMemberID));
        //            //Create_Room(roomnameIp.text/*, startTimeHR, startTimeMIN, endTimeHR, endTimeMIN, roomstate,"123"*/);
        //        }
        //    }
        //});
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator InsertRoom(int ID, string roomname, int startTime, int endTime, int roomstate, string member_id, string meetings_id)
    {
        WWWForm form = new WWWForm();
        form.AddField("idPost", ID);
        form.AddField("roomnamePost", roomname);
        form.AddField("starttimePost", startTime);
        form.AddField("endtimePost", endTime);
        form.AddField("roomstatePost", roomstate);
        form.AddField("memberidPost", member_id);
        form.AddField("meetingsidPost", meetings_id);

        using (UnityWebRequest www = UnityWebRequest.Post(CreateRoomURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
    /*public void Create_Room(string roomname, int startTimeHR, int startTimeMIN, int endTimeHR, int endTimeMIN, int roomstate, string member_id)
    {
        WWWForm form = new WWWForm();
        form.AddField("roomnamePost", roomname);
        form.AddField("starttimehrPost", startTimeHR);
        form.AddField("starttimeminPost", startTimeMIN);
        form.AddField("endtimehrPost", endTimeHR);
        form.AddField("endtimeminPost", endTimeMIN);
        form.AddField("roomstatePost", roomstate);
        form.AddField("memberidPost", member_id);

        WWW www = new WWW(CreateRoomURL, form);
    }*/
}
