﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomInfoLoader : MonoBehaviour
{
    public string[] rooms = { };

    IEnumerator Start()
    {
        WWW roominfo = new WWW("https://meetingroommanage.000webhostapp.com/roomInfo.php");
        yield return roominfo;
        string roominfoString = roominfo.text;
        print(roominfoString);
        rooms = roominfoString.Split(';');
        Debug.Log("roomLength = " + rooms.Length);
        Invoke("Update_invoke", 1f);
    }
    void Update_invoke()
    {
        StartCoroutine(UpdateDataBase());
        Invoke("Update_invoke", 1f);
    }
    public void UseStartcoroUpdate()
    {
        StartCoroutine(UpdateDataBase());
    }

    public IEnumerator UpdateDataBase()
    {
        WWW roominfo = new WWW("https://meetingroommanage.000webhostapp.com/roomInfo.php");
        yield return roominfo;
        string roominfoString = roominfo.text;
        print(roominfoString);
        rooms = roominfoString.Split(';');
        Debug.Log("roomLength = " + rooms.Length);
    }

    string GetDataValue(string data, string index)
    {
        if (data == "") return null;
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if (value.Contains("|")) value = value.Remove(value.IndexOf("|"));
        return value;
    }

    public int GetID(int index)
    {
        int tmp = int.Parse(GetDataValue(rooms[index], "ID:"));
        return tmp;
    }

    public int GetMaxIDNumber()
    {
        int max = 0;
        if (rooms.Length == 1) return max;
        max = int.Parse(GetDataValue(rooms[0], "ID:"));
        for (int i = 0; i < rooms.Length - 1; i++)
        {
            for(int j = i + 1; j < rooms.Length - 1; j++)
            {
                if(int.Parse(GetDataValue(rooms[j], "ID:")) > int.Parse(GetDataValue(rooms[i], "ID:")))
                {
                    max = int.Parse(GetDataValue(rooms[j], "ID:"));
                }
            }
        }
        return max;
    }

    public string GetRoomname(int index)
    {
        return GetDataValue(rooms[index], "roomname:");
    }

    public int GetStarttime(int index)
    {
        int tmp = int.Parse(GetDataValue(rooms[index], "starttime:"));
        return tmp;
    }

    public int GetEndtime(int index)
    {
        int tmp = int.Parse(GetDataValue(rooms[index], "endtime:"));
        return tmp;
    }

    public bool GetRoomstate(int index)
    {
        if(int.Parse(GetDataValue(rooms[index], "roomstate:")) == 1){
            return true;
        }
        return false;
    }

    public int GetRoomstateINT(int index)
    {
        return int.Parse(GetDataValue(rooms[index], "roomstate:"));
    }

    public string GetMember_id(int index)
    {
        return GetDataValue(rooms[index], "member_id:");
    }

    public string GetMeetings_id(int index)
    {
        return GetDataValue(rooms[index], "meetings_id:");
    }
}
