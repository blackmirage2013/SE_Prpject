﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UpdateRoom : MonoBehaviour
{
    public UpdateMeetings updateMeetings;
    string UpdateRoomURL = "https://meetingroommanage.000webhostapp.com/updateRoom.php";

    public void UseStartcoroUpdateRoom(int ID, string roomname, int startTime, int endTime, int roomstate, string member_id, string meetings_id)
    {
        StartCoroutine(updateRoom(ID, roomname, startTime, endTime, roomstate, member_id, meetings_id));
    }

    public IEnumerator updateRoom(int ID, string roomname, int startTime, int endTime, int roomstate, string member_id, string meetings_id)
    {
        WWWForm form = new WWWForm();
        form.AddField("idPost", ID);
        form.AddField("roomnamePost", roomname);
        form.AddField("starttimePost", startTime);
        form.AddField("endtimePost", endTime);
        form.AddField("roomstatePost", roomstate);
        form.AddField("memberidPost", member_id);
        form.AddField("meetingsidPost", meetings_id);

        using (UnityWebRequest www = UnityWebRequest.Post(UpdateRoomURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
        Debug.Log("[完成房間更新]");
    }
}
