﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class DeleteRoom : MonoBehaviour
{
    public DeleteMeeting deleteMeeting;
    public RoomInfoLoader roomInfoLoader;
    public meetingInfoLoader meetingInfoLoader;

    string DeleteRoomURL = "https://meetingroommanage.000webhostapp.com/deleteRoom.php";

    public IEnumerator deleteRoom(int ID)
    {
        for (int i = 0; i < roomInfoLoader.rooms.Length - 1; i++)
        {
            if (roomInfoLoader.GetID(i) == ID)
            {
                Debug.Log("HIIII");
                string[] meetingID = roomInfoLoader.GetMeetings_id(i).Split(',');
                Debug.Log("roomInfoLoader.GetMeetings_id(i) = "+roomInfoLoader.GetMeetings_id(i));
                Debug.Log("meetingID Length = " + meetingID.Length);
                for (int j = 0; j < meetingInfoLoader.meetings.Length - 1; j++)
                {
                    for (int k = 0; k < meetingID.Length - 1; k++)
                    {
                        if (meetingInfoLoader.GetID(j) == meetingID[k])
                        {
                            Debug.Log("meetingID = "+meetingInfoLoader.GetID(j));
                            
                            StartCoroutine(deleteMeeting.deleteMeeting(int.Parse(meetingInfoLoader.GetID(j))));
                            break;
                        }
                    }
                }
            }
        }
        WWWForm form = new WWWForm();
        form.AddField("idPost", ID);

        using (UnityWebRequest www = UnityWebRequest.Post(DeleteRoomURL, form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
            }
        }
    }
}
