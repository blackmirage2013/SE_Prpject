﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HostRoomInfo : MonoBehaviour
{
    [HideInInspector]
    [SerializeField] private MeetingReserve meetingReserve;
    [SerializeField] private InputField meetingName_input;
    [SerializeField] private Text roomSelect_text;
    [SerializeField] private InputField maxMemberAmount_input;
    [SerializeField] private InputField meetingInfo_text;
    [SerializeField] private DatePicker datePicker;
    [SerializeField] private Text timeStart;
    [SerializeField] private Text timeEnd;
    [SerializeField] private UpdateMeetings updateMeetings;
    [SerializeField] private meetingInfoLoader meetingInfoLoader;
    [SerializeField] private DataLoader userInfoLoader;
    [SerializeField] private DeleteMeeting deleteMeeting;


    //[SerializeField] private GameObject memberPrefab;
    //[SerializeField] private Transform memberContext;

    [SerializeField] private Transform memberSlot;
    [SerializeField] private Transform inviteSlot;
    [SerializeField] private GameObject UserInfo;

    private MeetingData OriginalMeetingData;
    private MeetingData meetingData;
    //private int roomSelect_DropdownIndex = 0;
    private bool enableButton_Member = false;
    private bool enableButton_Invite = false;
    private bool setTimeStartOrEnd = true;
    List<string> invite=new List<string>();
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }
    private void OnEnable()
    {
        invite.Clear();
    }
    public void SetInfo(MeetingData _meetingData)
    {
        enableButton_Member = false;
        enableButton_Invite = false;
        AccountManager.instance.accountType = AccountType.Host;
        for (int i = memberSlot.childCount - 1; i >= 0; i--)
            Destroy(memberSlot.GetChild(i).gameObject);
        for (int i = inviteSlot.childCount - 1; i >= 0; i--)
            Destroy(inviteSlot.GetChild(i).gameObject);
        foreach (string temp in _meetingData.invite)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, inviteSlot).GetComponent<AccountInfo>().Copy(info);
        }
        foreach (string temp in _meetingData.member)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, memberSlot).GetComponent<AccountInfo>().Copy(info);
        }

        gameObject.SetActive(true);
        OriginalMeetingData = new MeetingData(_meetingData, _meetingData.host);
        meetingData = new MeetingData(_meetingData, SysyemManager.SM.UserEmail);
        meetingName_input.text = _meetingData.meetingName;
        roomSelect_text.text = _meetingData.roomName;
        maxMemberAmount_input.text = _meetingData.maxMemberAmount.ToString();
        meetingInfo_text.text = _meetingData.meetingInfo;
        timeStart.text = _meetingData.meetingTime.startTime.Year.ToString() + "年" + _meetingData.meetingTime.startTime.Month.ToString() + "月" + _meetingData.meetingTime.startTime.Day.ToString() + "日";
        timeStart.text += _meetingData.meetingTime.startTime.Hour.ToString() + "點";
        timeStart.text += _meetingData.meetingTime.startTime.Minute.ToString() + "分";
        timeEnd.text = _meetingData.meetingTime.endTime.Year.ToString() + "年" + _meetingData.meetingTime.endTime.Month.ToString() + "月" + _meetingData.meetingTime.endTime.Day.ToString() + "日";
        timeEnd.text += _meetingData.meetingTime.endTime.Hour.ToString() + "點";
        timeEnd.text += _meetingData.meetingTime.endTime.Minute.ToString() + "分";
    }
    public void SetMeetingName(string _text)
    {
        meetingData.meetingName = _text;
    }
    public void SetMemberAmount(string _text)
    {
        int m = meetingData.maxMemberAmount;
        try
        {
            m = int.Parse(_text);
        }
        catch (System.FormatException)
        {
            Debug.LogError("容納數量需輸入數字");
        }
        meetingData.maxMemberAmount = m;
        maxMemberAmount_input.text = m.ToString();
    }
    public void SetMeetingInfo(string _text)
    {
        meetingData.meetingInfo = _text;
    }
    public void SetTimeDay(bool _bool)
    {
        datePicker.gameObject.SetActive(true);
        setTimeStartOrEnd = _bool;
    }
    public void ConfirmSetTime()
    {
        if (setTimeStartOrEnd)
        {
            timeStart.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingData.meetingTime.startTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingData.meetingTime.startTime.Hour, meetingData.meetingTime.startTime.Minute, 0);
        }
        else
        {
            timeEnd.text = datePicker.Date.date.Year.ToString() + "年" + datePicker.Date.date.Month.ToString() + "月" + datePicker.Date.date.Day.ToString() + "日";
            meetingData.meetingTime.endTime = new System.DateTime(datePicker.Date.date.Year, datePicker.Date.date.Month, datePicker.Date.date.Day, meetingData.meetingTime.endTime.Hour, meetingData.meetingTime.endTime.Minute, 0);
        }
        datePicker.gameObject.SetActive(false);
    }
    public void AddInvite(AccountInfo info)
    {
        if (meetingData.AddInvited(info.mailText.text))
        {
            invite.Add(info.mailText.text);
            Instantiate(UserInfo, inviteSlot).GetComponent<AccountInfo>().Copy(info);
        }
    }

    public void ChangeMeeting()//更改會議資訊
    {
        OriginalMeetingData = new MeetingData(meetingData, meetingData.host);

        RoomManager.instance.FindRoomWithName(OriginalMeetingData.roomName).SetMeetingData(OriginalMeetingData.meetingTime.startTime, OriginalMeetingData);
        EmailSender.IE_SendEmail(invite,OriginalMeetingData.roomName, OriginalMeetingData.meetingName, OriginalMeetingData.meetingInfo);

        string startTimeStr = OriginalMeetingData.meetingTime.startTime.Year.ToString() + "-";
        if (OriginalMeetingData.meetingTime.startTime.Month > 9)//10~12月
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Month.ToString();
        }
        else//1~9月
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Month.ToString();
        }
        startTimeStr += "-";
        if(OriginalMeetingData.meetingTime.startTime.Day > 9)//10~31號
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Day.ToString();
        }
        else//0~9號
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Day.ToString();
        }
        startTimeStr += " ";
        if (OriginalMeetingData.meetingTime.startTime.Hour > 9)//10~24點
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Hour.ToString();
        }
        else//0~9點
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Hour.ToString();
        }
        startTimeStr += ":";
        if (OriginalMeetingData.meetingTime.startTime.Minute > 9)//10~59分
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Minute.ToString();
        }
        else//0~9分
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Minute.ToString();
        }
        startTimeStr += ":00";

        string memberID = "", inviteID = "", applyID = "";
        for(int i = 0; i < OriginalMeetingData.member.Count; i++)
        {
            for(int j = 0; j < userInfoLoader.participators.Length - 1; j++)
            {
                if (userInfoLoader.GetMail(j) == OriginalMeetingData.member[i])
                {
                    memberID += userInfoLoader.GetID(j);
                    memberID += ",";
                    break;
                }
            }
        }
        for (int i = 0; i < OriginalMeetingData.invite.Count; i++)
        {
            for (int j = 0; j < userInfoLoader.participators.Length - 1; j++)
            {
                if (userInfoLoader.GetMail(j) == OriginalMeetingData.invite[i])
                {
                    inviteID += userInfoLoader.GetID(j);
                    inviteID += ",";
                    break;
                }
            }
        }
        for (int i = 0; i < OriginalMeetingData.require.Count; i++)
        {
            for (int j = 0; j < userInfoLoader.participators.Length - 1; j++)
            {
                if (userInfoLoader.GetMail(j) == OriginalMeetingData.require[i])
                {
                    applyID += userInfoLoader.GetID(j);
                    applyID += ",";
                    break;
                }
            }
        }
        for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
        {
            if (meetingInfoLoader.GetHost(i) == OriginalMeetingData.host && meetingInfoLoader.GetStarttime(i) == startTimeStr)
            {
                updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), OriginalMeetingData.meetingName, OriginalMeetingData.roomName, OriginalMeetingData.meetingInfo, meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), OriginalMeetingData.maxMemberAmount, OriginalMeetingData.host, memberID, inviteID, applyID);
                break;
            }
        }

        SetInfo(OriginalMeetingData);
    }

    public void ClickToEditInvite()
    {
        enableButton_Invite = !enableButton_Invite;
        for (int i = 0; i < inviteSlot.childCount; i++)
        {
            inviteSlot.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_Invite);
        }

        enableButton_Member = false;
        for (int i = 0; i < memberSlot.childCount; i++)
        {
            memberSlot.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_Member);
        }
    }
    public void ClickToEditMember()
    {
        enableButton_Member = !enableButton_Member;
        for (int i = 0; i < memberSlot.childCount; i++)
        {
            memberSlot.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_Member);
        }

        enableButton_Invite = false;
        for (int i = 0; i < inviteSlot.childCount; i++)
        {
            inviteSlot.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_Invite);
        }
    }
    public void RemoveUser(AccountInfo info)
    {
        if (enableButton_Invite)
        {
            meetingData.RemoveInvited(info.mailText.text);
            invite.Remove(info.mailText.text);      
        }
        else if (enableButton_Member)
        {
            meetingData.Leave(info.mailText.text);
        }
        else
            Debug.LogWarning("例外出現需處理!");
    }
    public void CancelReserve()//取消預約 DELETE
    {
        RoomManager.instance.FindRoomWithName(OriginalMeetingData.roomName)?.RemoveMeetingData(OriginalMeetingData);
        invite.Clear();

        string startTimeStr = OriginalMeetingData.meetingTime.startTime.Year.ToString() + "-";
        if (OriginalMeetingData.meetingTime.startTime.Month > 9)//10~12月
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Month.ToString();
        }
        else//1~9月
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Month.ToString();
        }
        startTimeStr += "-";
        if (OriginalMeetingData.meetingTime.startTime.Day > 9)//10~31號
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Day.ToString();
        }
        else//0~9號
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Day.ToString();
        }
        startTimeStr += " ";
        if (OriginalMeetingData.meetingTime.startTime.Hour > 9)//10~24點
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Hour.ToString();
        }
        else//0~9點
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Hour.ToString();
        }
        startTimeStr += ":";
        if (OriginalMeetingData.meetingTime.startTime.Minute > 9)//10~59分
        {
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Minute.ToString();
        }
        else//0~9分
        {
            startTimeStr += "0";
            startTimeStr += OriginalMeetingData.meetingTime.startTime.Minute.ToString();
        }
        startTimeStr += ":00";
        
        for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
        {
            if (meetingInfoLoader.GetRoomname(i) == OriginalMeetingData.roomName && meetingInfoLoader.GetHost(i) == OriginalMeetingData.host && meetingInfoLoader.GetStarttime(i) == startTimeStr)
            {
                RoomManager.instance.SaveRoomData(RoomManager.instance.FindRoomWithName(OriginalMeetingData.roomName));
                StartCoroutine(deleteMeeting.deleteMeeting(int.Parse(meetingInfoLoader.GetID(i))));
                break;
            }
        }

        RoomManager.instance.DelaySave(RoomManager.instance.FindRoomWithName(OriginalMeetingData.roomName));
        gameObject.SetActive(false);
    }
}
