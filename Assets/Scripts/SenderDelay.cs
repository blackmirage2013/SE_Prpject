﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SenderDelay : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        EmailSender.senderDelay = this;
        EmailSender.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Delay_sendEmail()
    {
        Invoke("CellEmailSender", 1f);
    }
    public void CellEmailSender()
    {
        EmailSender.Delay_sendEmail();
    }
}
