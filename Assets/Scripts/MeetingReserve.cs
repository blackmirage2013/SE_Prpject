﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeetingReserve : MonoBehaviour
{
    public List<RoomData> roomDatas=new List<RoomData>();

    public void CancelReserve(MeetingData _meetingData)
    {
        for (int i = 0; i < roomDatas.Count; i++)
        {
            if (roomDatas[i].roomName == _meetingData.roomName)
            {
                for (int j = 0; j < roomDatas[i].meetings.Count; j++)//將指定會議移除
                {
                    //Debug.Log("內容物"+roomDatas[i].meetings[j].meetingTime.startTime);
                    if (roomDatas[i].meetings[j].meetingTime.startTime == _meetingData.meetingTime.startTime)
                    {
                        //還需要加入成員通知
                        roomDatas[i].meetings.RemoveAt(j);
                        Debug.Log("移除成功!");
                        return;
                    }
                }
            }
        }
        Debug.Log("例外狀況移除失敗!");
        return;
    }
    public bool Reserve(MeetingData _meetingData)
    {
        if (_meetingData.meetingTime.startTime>= _meetingData.meetingTime.endTime|| _meetingData.meetingTime.startTime<DateTime.Now)//防呆 開始大於等於結束時間 或是開始時間比現在早
        {
            Debug.LogWarning("輸入時間錯誤!");
            return false;
        }

        for (int i = 0; i < roomDatas.Count; i++)
        {
            if (roomDatas[i].roomName == _meetingData.roomName)
            {
                //檢查是否有權限開會議

                foreach (var meetingData in roomDatas[i].meetings)
                {
                    if ((_meetingData.meetingTime.startTime<= meetingData.meetingTime.startTime&& meetingData.meetingTime.startTime < _meetingData.meetingTime.endTime)||
                        (_meetingData.meetingTime.startTime < meetingData.meetingTime.endTime && meetingData.meetingTime.endTime <= _meetingData.meetingTime.endTime))
                    {
                        Debug.LogWarning("該時段已有人預約了!");
                        return false;
                    }
                }
                roomDatas[i].meetings.Add(_meetingData);
                Debug.Log("預約成功!");
                return true;
            }
        }
        Debug.LogWarning("找不到名為\"" + _meetingData.roomName + "\"的房間!");
        return false;
    }

    private void AddMeeting2Room(int _position, MeetingData _meetingData)
    {
        for (int i = 0; i < roomDatas[_position].meetings.Count; i++)
        {
            if (roomDatas[_position].meetings[i].meetingTime.startTime> _meetingData.meetingTime.startTime)
            {
                roomDatas[_position].meetings.Insert(i, _meetingData);
                return;
            }
        }
        roomDatas[_position].meetings.Add(_meetingData);    
    }
    public void MoveMember(/*人員class,*/MoveMemberWay _moveMemberWay)
    {
        switch (_moveMemberWay)
        {
            case MoveMemberWay.AcceptInvitation://受邀者接受邀請
                break;
            case MoveMemberWay.NotAcceptInvitation://受邀者拒絕邀請
                break;
            case MoveMemberWay.AddInvitation://host邀請加入
                break;
            case MoveMemberWay.RemoveInvitation://host取消邀請
                break;
            case MoveMemberWay.Agree://host同意加入
                break;
            case MoveMemberWay.Disagree://host不同意加入
                break;
            case MoveMemberWay.Leave://將成員移除(或是成員自己離開)
                break;
        }
    }

}
public enum MoveMemberWay
{
    AcceptInvitation,
    NotAcceptInvitation,
    AddInvitation,
    RemoveInvitation,
    Agree,
    Disagree,
    Leave
}
[System.Serializable]
public class MeetingData
{
    public string meetingName;
    public string roomName;
    public string meetingInfo;
    public MeetingTime meetingTime;
    public int maxMemberAmount;
    public string host;
    public List<string> member;
    public List<string> invite;
    public List<string> require;
    public MeetingData(MeetingData _meetingData, string _hostEmail)
    {
        meetingName = _meetingData.meetingName;
        roomName = _meetingData.roomName;
        meetingInfo = _meetingData.meetingInfo;
        meetingTime = new MeetingTime(_meetingData.meetingTime.startTime, _meetingData.meetingTime.endTime);
        maxMemberAmount = _meetingData.maxMemberAmount;
        host = _hostEmail;
        member = new List<string>(_meetingData.member);
        invite = new List<string>(_meetingData.invite);
        require = new List<string>(_meetingData.require);
    }
    public MeetingData(string _meetingName, string _roomName, string _meetingInfo, DateTime _startTime, DateTime _endTime, int _maxMemberAmount, string _hostEmail)
    {
        meetingName = _meetingName;
        roomName = _roomName;
        meetingInfo = _meetingInfo;
        meetingTime = new MeetingTime(_startTime, _endTime);
        maxMemberAmount = _maxMemberAmount;
        host = _hostEmail;
        member = new List<string>();
        invite = new List<string>();
        require = new List<string>();
    }
    public MeetingData(string _meetingName, string _roomName, string _meetingInfo, MeetingTime _meetingTime, int _maxMemberAmount, string _hostEmail)
    {
        meetingName = _meetingName;
        roomName = _roomName;
        meetingInfo = _meetingInfo;
        meetingTime = _meetingTime;
        maxMemberAmount = _maxMemberAmount;
        host = _hostEmail;
        member = new List<string>();
        invite = new List<string>();
        require = new List<string>();
    }
    // for 資料庫讀檔並建構
    public MeetingData(string _meetingName, string _roomName, string _meetingInfo, MeetingTime _meetingTime, int _maxMemberAmount, 
        string _hostEmail, List<string> members, List<string> invites, List<string> requires)
    {
        meetingName = _meetingName;
        roomName = _roomName;
        meetingInfo = _meetingInfo;
        meetingTime = _meetingTime;
        maxMemberAmount = _maxMemberAmount;
        host = _hostEmail;
        member = members;
        invite = invites;
        require = requires;
        
    }
    public bool AddRequire(string _userEmail)
    {
        foreach (string memberTemp in this.member)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經是成員了!");
                return false;
            }
        }
        foreach (string memberTemp in this.invite)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經被邀請了!");
                return false;
            }
        }
        foreach (string memberTemp in this.require)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經寄送過請求了!");
                return false;
            }
        }
        require.Add(_userEmail);
        return true;
    }
    public bool RemoveRequire(string _userEmail)
    {
        for(int i=0;i< require.Count;i++)
        {
            if (require[i] == _userEmail)
            {
                require.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    public bool AcceptRequire(string _userEmail)
    {
        if (RemoveRequire(_userEmail))
        {
            member.Add(_userEmail);
            return true;
        }
        else
            return false;
    }
    public bool AddInvited(string _userEmail)
    {
        foreach (string memberTemp in this.member)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經是成員了!");
                return false;
            }
        }
        foreach (string memberTemp in this.invite)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經被邀請了!");
                return false;
            }
        }
        foreach (string memberTemp in this.require)
        {
            if (_userEmail == memberTemp)
            {
                Debug.LogWarning("你已經寄送過請求了!");
                return false;
            }
        }
        if (_userEmail == host)
        {
            Debug.LogWarning("你是主持人!");
            return false;
        }

        invite.Add(_userEmail);
        return true;
    }
    public bool RemoveInvited(string _userEmail)
    {
        for (int i = 0; i < invite.Count; i++)
        {
            if (invite[i] == _userEmail)
            {
                invite.RemoveAt(i);
                return true;
            }
        }
        return false;
    }
    public bool AcceptInvited(string _userEmail)
    {
        if (RemoveInvited(_userEmail))
        {
            member.Add(_userEmail);
            return true;
        }
        return false;
    }
    public void Leave(string _userEmail)
    {
        for (int i = 0; i < member.Count; i++)
        {
            if (member[i] == _userEmail)
            {
                member.RemoveAt(i);
            }
        }
    }
    public bool SearchMember(string _userEmail)
    {
        if (_userEmail==host)
            return true;
        foreach (string memberTemp in this.member)
        {
            if (_userEmail == memberTemp)
            {
                return true;
            }
        }
        foreach (string memberTemp in this.invite)
        {
            if (_userEmail == memberTemp)
            {
                return true;
            }
        }
        foreach (string memberTemp in this.require)
        {
            if (_userEmail == memberTemp)
            {
                return true;
            }
        }
        return false;
    }
}
[System.Serializable]
public class RoomData
{
    public string roomName;
    public List<MeetingData> meetings;

    public RoomData(string _roomName)
    {
        roomName = _roomName;
        meetings = new List<MeetingData>();
    }
}

[System.Serializable]
public class MeetingTime
{
    public DateTime startTime;
    public DateTime endTime;

    public MeetingTime(DateTime _time)
    {
        startTime = _time;
        endTime = _time;
    }
    public MeetingTime(DateTime _startTime, DateTime _endTime)
    {
        startTime = _startTime;
        endTime = _endTime;
    }
}