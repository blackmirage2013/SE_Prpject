﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class SearchSlot : MonoBehaviour
{
    private MeetingData meetingData;
    [SerializeField] private ListenerRoomInfo Listener_Window;
    [SerializeField] private StaffRoomInfo Staff_Window;
    [SerializeField] private HostRoomInfo Host_Window;
    public Text slotText;

    void Start()
    {
        //SetTime(DateTime.Now, DateTime.Now);
        Listener_Window = SysyemManager.SM.Listener_Window;
        Staff_Window = SysyemManager.SM.Staff_Window;
        Host_Window = SysyemManager.SM.Host_Window;
        gameObject.GetComponent<Button>().onClick.AddListener(() => OpenWindow());
    }

    public void SetMeetingData(MeetingData _meetingData)
    {
        meetingData =new MeetingData(_meetingData, _meetingData.host);
    }
    private void OpenWindow()
    {
        if (SysyemManager.SM.UserEmail == meetingData.host)
        {
            Host_Window.SetInfo(meetingData);
            return;
        }

        foreach (string memberEmail in meetingData.member)
        {
            if (SysyemManager.SM.UserEmail == memberEmail)
            {
                Staff_Window.SetInfo(meetingData);
                return;
            }
        }

        foreach (string memberEmail in meetingData.invite)
        {
            if (SysyemManager.SM.UserEmail == memberEmail)
            {
                Listener_Window.SetInfo(meetingData, Identity.Invited);
                return;
            }
        }

        foreach (string memberEmail in meetingData.require)
        {
            if (SysyemManager.SM.UserEmail == memberEmail)
            {
                Listener_Window.SetInfo(meetingData, Identity.Require);
                return;
            }
        }
        Listener_Window.SetInfo(meetingData, Identity.NotRequire);
    }
}
