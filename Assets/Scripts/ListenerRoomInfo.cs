﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListenerRoomInfo : MonoBehaviour
{
    [HideInInspector]
    [SerializeField] private MeetingReserve meetingReserve;
    [SerializeField] private StaffRoomInfo Staff_Window;
    [SerializeField] private Text meetingName;
    [SerializeField] private Text roomSelect;
    [SerializeField] private Text maxMemberAmount;
    [SerializeField] private Text meetingInfo;
    [SerializeField] private Text timeStart;
    [SerializeField] private Text timeEnd;
    [SerializeField] private GameObject require;
    [SerializeField] private GameObject cancelRequire;
    [SerializeField] private GameObject accept;
    [SerializeField] private GameObject NotAccept;
    [SerializeField] private Transform memberSlot;
    [SerializeField] private Transform inviteSlot;
    [SerializeField] private GameObject UserInfo;
    [SerializeField] private UpdateMeetings updateMeetings;
    [SerializeField] private meetingInfoLoader meetingInfoLoader;
    [SerializeField] private DataLoader userInfoLoader;
    private MeetingData meetingData;


    public void SetInfo(MeetingData _meetingData, Identity _identity)
    {
        for (int i = memberSlot.childCount - 1; i >= 0; i--)
            Destroy(memberSlot.GetChild(i).gameObject);
        for (int i = inviteSlot.childCount - 1; i >= 0; i--)
            Destroy(inviteSlot.GetChild(i).gameObject);
        foreach (string temp in _meetingData.invite)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, inviteSlot).GetComponent<AccountInfo>().Copy(info);
        }
        foreach (string temp in _meetingData.member)
        {
            AccountInfo info = AccountManager.instance.GetAccountInfo(temp);
            if (info != null)
                Instantiate(UserInfo, memberSlot).GetComponent<AccountInfo>().Copy(info);
        }

        meetingData = _meetingData;
        gameObject.SetActive(true);
        meetingName.text = _meetingData.meetingName;
        roomSelect.text = _meetingData.roomName;
        maxMemberAmount.text = _meetingData.maxMemberAmount.ToString();
        meetingInfo.text = _meetingData.meetingInfo;
        timeStart.text = _meetingData.meetingTime.startTime.Year.ToString() + "年" + _meetingData.meetingTime.startTime.Month.ToString() + "月" + _meetingData.meetingTime.startTime.Day.ToString() + "日";
        timeStart.text += _meetingData.meetingTime.startTime.Hour.ToString() + "點";
        timeStart.text += _meetingData.meetingTime.startTime.Minute.ToString() + "分";
        timeEnd.text = _meetingData.meetingTime.endTime.Year.ToString() + "年" + _meetingData.meetingTime.endTime.Month.ToString() + "月" + _meetingData.meetingTime.endTime.Day.ToString() + "日";
        timeEnd.text += _meetingData.meetingTime.endTime.Hour.ToString() + "點";
        timeEnd.text += _meetingData.meetingTime.endTime.Minute.ToString() + "分";

        SetIdentity(_identity);
    }
    private void SetIdentity(Identity _identity)
    {
        require.SetActive(false);
        cancelRequire.SetActive(false);
        accept.SetActive(false);
        NotAccept.SetActive(false);
        switch (_identity)
        {
            case Identity.Invited:
                accept.SetActive(true);
                NotAccept.SetActive(true);
                return;
            case Identity.Require:
                cancelRequire.SetActive(true);
                return;
            case Identity.NotRequire:
                require.SetActive(true);
                return;
        }
    }

    public void AcceptInvitation()
    {
        //同意加入會議
        MeetingData meetingDataTemp = RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime);
        if (meetingDataTemp == null)
        {
            gameObject.SetActive(false);
            Debug.LogWarning("此會議不存在!或已遭刪除!");
            return;
        }
        else if (meetingDataTemp.AcceptInvited(SysyemManager.SM.UserEmail))
        {
            EmailSender.IE_Accept(meetingDataTemp.host, meetingDataTemp.roomName, meetingDataTemp.meetingName, meetingDataTemp.meetingInfo);
            gameObject.SetActive(false);
            Staff_Window.SetInfo(RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime));
            string userID = "";
            for(int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
            {
                if(userInfoLoader.GetMail(i) == SysyemManager.SM.UserEmail)
                {
                    userID = userInfoLoader.GetID(i);
                    break;
                }
            }
            for(int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if(meetingInfoLoader.GetMeetingname(i)==meetingName.text&&meetingInfoLoader.GetRoomname(i)== roomSelect.text&&meetingInfoLoader.GetMeetinginfo(i)== meetingInfo.text && meetingInfoLoader.GetMaxmemberamount(i) == int.Parse(maxMemberAmount.text))
                {
                    string memberID = meetingInfoLoader.GetMember(i) + userID + ",";//將此成員加入參加會議成員名單
                    string inviteID = meetingInfoLoader.GetInvite(i).Remove(meetingInfoLoader.GetInvite(i).IndexOf(userID), 2);//從invite中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), memberID, inviteID, meetingInfoLoader.GetApply(i));
                    break;
                }
            }
        }
    }
    public void NotAcceptInvitation()
    {
        //不同意加入會議
        MeetingData meetingDataTemp = RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime);
        if (meetingDataTemp == null)
        {
            gameObject.SetActive(false);
            Debug.LogWarning("此會議不存在!或已遭刪除!");
            return;
        }
        else if (meetingDataTemp.RemoveInvited(SysyemManager.SM.UserEmail))
        {
            SetIdentity(Identity.NotRequire);
            SetInfo(meetingDataTemp, Identity.NotRequire);
            string userID = "";
            for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
            {
                if (userInfoLoader.GetMail(i) == SysyemManager.SM.UserEmail)
                {
                    userID = userInfoLoader.GetID(i);
                    break;
                }
            }
            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetMeetingname(i) == meetingName.text && meetingInfoLoader.GetRoomname(i) == roomSelect.text && meetingInfoLoader.GetMeetinginfo(i) == meetingInfo.text && meetingInfoLoader.GetMaxmemberamount(i) == int.Parse(maxMemberAmount.text))
                {
                    string inviteID = meetingInfoLoader.GetInvite(i).Remove(meetingInfoLoader.GetInvite(i).IndexOf(userID), 2);//從invite中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), meetingInfoLoader.GetMember(i), inviteID, meetingInfoLoader.GetApply(i));
                    break;
                }
            }
        }
    }
    public void Require()
    {
        //請求加入會議
        if (meetingData.AddRequire(SysyemManager.SM.UserEmail))
        {
            MeetingData meetingDataTemp = RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime);
            //SetIdentity(Identity.Require);
            if (meetingDataTemp == null)
            {
                gameObject.SetActive(false);
                Debug.LogWarning("此會議不存在!或已遭刪除!");
                return;
            }
            SetInfo(meetingDataTemp, Identity.Require);
            string userID = "";
            for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
            {
                if (userInfoLoader.GetMail(i) == SysyemManager.SM.UserEmail)
                {
                    userID = userInfoLoader.GetID(i);
                    break;
                }
            }
            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetMeetingname(i) == meetingName.text && meetingInfoLoader.GetRoomname(i) == roomSelect.text && meetingInfoLoader.GetMeetinginfo(i) == meetingInfo.text && meetingInfoLoader.GetMaxmemberamount(i) == int.Parse(maxMemberAmount.text))
                {
                    string applyID = meetingInfoLoader.GetApply(i) + userID + ",";//將此成員加入apply中
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), meetingInfoLoader.GetMember(i), meetingInfoLoader.GetInvite(i), applyID);
                    break;
                }
            }
        }
    }
    public void CancelRequire()
    {
        //取消請求加入會議
        if (meetingData.RemoveRequire(SysyemManager.SM.UserEmail))
        {
            MeetingData meetingDataTemp = RoomManager.instance.FindRoomWithName(meetingData.roomName).FindMeetingData(meetingData.meetingTime.startTime);
            //SetIdentity(Identity.NotRequire);
            if (meetingDataTemp == null)
            {
                gameObject.SetActive(false);
                Debug.LogWarning("此會議不存在!或已遭刪除!");
                return;
            }
            SetInfo(meetingDataTemp, Identity.NotRequire);
            string userID = "";
            for (int i = 0; i < userInfoLoader.participators.Length - 1; i++)//尋找此成員的ID
            {
                if (userInfoLoader.GetMail(i) == SysyemManager.SM.UserEmail)
                {
                    userID = userInfoLoader.GetID(i);
                    break;
                }
            }
            for (int i = 0; i < meetingInfoLoader.meetings.Length - 1; i++)
            {
                if (meetingInfoLoader.GetMeetingname(i) == meetingName.text && meetingInfoLoader.GetRoomname(i) == roomSelect.text && meetingInfoLoader.GetMeetinginfo(i) == meetingInfo.text && meetingInfoLoader.GetMaxmemberamount(i) == int.Parse(maxMemberAmount.text))
                {
                    string applyID = meetingInfoLoader.GetApply(i).Remove(meetingInfoLoader.GetApply(i).IndexOf(userID), 2);//從apply中移除此成員
                    updateMeetings.UseStartcoroUpdateMeeting(int.Parse(meetingInfoLoader.GetID(i)), meetingInfoLoader.GetMeetingname(i), meetingInfoLoader.GetRoomname(i), meetingInfoLoader.GetMeetinginfo(i), meetingInfoLoader.GetStarttime(i), meetingInfoLoader.GetEndtime(i), meetingInfoLoader.GetMaxmemberamount(i), meetingInfoLoader.GetHost(i), meetingInfoLoader.GetMember(i), meetingInfoLoader.GetInvite(i), applyID);
                    break;
                }
            }
        }
    }
}
public enum Identity
{
    Invited,
    Require,
    NotRequire
}
