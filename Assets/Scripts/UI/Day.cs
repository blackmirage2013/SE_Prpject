﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class Day : MonoBehaviour
{
    public DateTime date;
    public DatePicker datePicker;
    public Text output;

    public void OnClickDate()
    {
        datePicker.Date = this;
    }
}
