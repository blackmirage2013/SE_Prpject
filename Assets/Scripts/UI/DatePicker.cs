﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class DatePicker : MonoBehaviour
{
    
    private Day date;
    public Day Date
    {
        get { return date; }
        set
        {
            if (date != null)
                date.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
            date = value;
            date.GetComponent<Image>().color = new Color32(128, 255, 128, 255);
            Debug.Log(date.date.Day);
        }
    }

    public InputField year;
    public InputField month;
    public List<Day> days;

    // Start is called before the first frame update
    private void OnEnable()
    {
        init();
        OnChange(DateTime.Now.Year, DateTime.Now.Month);
    }

    void ClearCalendar()
    {
        for (int i = 0; i < 42; i++)
        {
            days[i].output.text = "";
            days[i].GetComponent<Button>().interactable = false;
        }
    }
    void init()
    {
        for (int i = 0; i < 42; i++)
        {
            days[i].datePicker = this;
        }
    }

    public void OnChange(int y, int m)
    {
        
        ClearCalendar();
        year.text = y.ToString();
        month.text = m.ToString();
        int ds = DateTime.DaysInMonth(y, m);
        DateTime fd = new DateTime(y, m, 1);
        int week = int.Parse(fd.DayOfWeek.ToString("d"));
        for (int i = week; i < week + ds; i++)
        {
            days[i].output.text = (i - week + 1).ToString();
            days[i].date = new DateTime(y, m, i - week + 1);
            days[i].GetComponent<Button>().interactable = true;
        }
        Date = days[week];
    }

    public void OnChange()
    {
        ClearCalendar();
        int y = int.Parse(year.text);
        int m = int.Parse(month.text);
        int ds = DateTime.DaysInMonth(y, m);
        DateTime fd = new DateTime(y, m, 1);
        int week = int.Parse(fd.DayOfWeek.ToString("d"));
        for (int i = week; i < week + ds; i++)
        {
            days[i].output.text = (i - week + 1).ToString();
            days[i].date = new DateTime(y, m, i - week + 1);
            days[i].GetComponent<Button>().interactable = true;
        }
        Date = days[week];
    }

    bool OutOfRange()
    {
        int ny = DateTime.Now.Year;
        int y = int.Parse(year.text);
        int m = int.Parse(month.text);
        return y <= ny - 30 || y >= ny + 30 || m < 1 || m > 12;
    }

    public void OnYearChange()
    {
        int ny = DateTime.Now.Year;
        int y = int.Parse(year.text);
        if (OutOfRange())
            year.text = ny.ToString();
        OnChange();
    }

    public void OnMonthChange()
    {
        int m = int.Parse(month.text);
        if (OutOfRange())
            month.text = DateTime.Now.Month.ToString();
        OnChange();
    }

    public void OnClickNextMonth()
    {
        int y = int.Parse(year.text);
        int m = int.Parse(month.text);
        if (m==12)
        {
            m = 1;
            y++;
        }
        else
            m++;
        if (!OutOfRange())
        {
            year.text = y.ToString();
            month.text = m.ToString();
            OnChange();
        }
    }

    public void OnClickLastMonth()
    {
        int y = int.Parse(year.text);
        int m = int.Parse(month.text);
        if (m == 1)
        {
            m = 12;
            y--;
        }
        else
            m--;
        if (!OutOfRange())
        {
            year.text = y.ToString();
            month.text = m.ToString();
            OnChange();
        }
    }
}
