﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class MeetingBox : MonoBehaviour
{
    private RectTransform rect;
    private DayRow dayRow;
    private float parentRect_Height = 1010f;
    private float timeconstant = 50;
    private float boxconstant = 40;
    private MeetingData meetingData;
    [SerializeField] private ListenerRoomInfo Listener_Window;
    [SerializeField] private StaffRoomInfo Staff_Window;
    [SerializeField] private HostRoomInfo Host_Window;

    void Start()
    {
        //SetTime(DateTime.Now, DateTime.Now);
        Listener_Window = SysyemManager.SM.Listener_Window;
        Staff_Window = SysyemManager.SM.Staff_Window;
        Host_Window = SysyemManager.SM.Host_Window;
        gameObject.GetComponent<Button>().onClick.AddListener(() => OpenWindow());
    }

    public void SetMeetingData(MeetingData _meetingData)
    {
        meetingData = _meetingData;
        SetTime();
    }
    public void SetTime()
    {
        DateTime st = meetingData.meetingTime.startTime;
        DateTime et = meetingData.meetingTime.endTime;
        rect = GetComponent<RectTransform>();
        DatePicker dp = GameObject.FindGameObjectWithTag("DatePickerMain").GetComponent<DatePicker>();

        int s_hour = st.Hour;
        int s_min = st.Minute;
        int e_hour = et.Hour;
        int e_min = et.Minute;
        if (st.Date != et.Date && dp.Date.date == st.Date && dp.Date.date != et.Date)
        {
            e_hour = 24;
            e_min = 0;
        }
        else if (st.Date != et.Date && dp.Date.date == et.Date && dp.Date.date != st.Date)
        {
            s_hour = 0;
            s_min = 0;
        }
        else if (st.Date != et.Date && dp.Date.date < et.Date && dp.Date.date > st.Date)
        {
            s_hour = 0;
            s_min = 0;
            e_hour = 24;
            e_min = 0;
        }
        float s_ratio = (float)s_hour + (float)s_min / 60.0f;
        float s_pos = timeconstant + s_ratio * boxconstant;

        float e_ratio = (float)e_hour + (float)e_min / 60.0f;
        float e_pos = parentRect_Height - (timeconstant + (e_ratio) * boxconstant);

        rect.offsetMin = new Vector2(rect.offsetMin.x, e_pos);
        rect.offsetMax = new Vector2(rect.offsetMax.x, -s_pos);
    }

    private void OpenWindow()
    {
        if (SysyemManager.SM.UserEmail==meetingData.host)
        {
            Host_Window.SetInfo(meetingData);
            return;
        }

        foreach (string memberEmail in meetingData.member)
        {
            if (SysyemManager.SM.UserEmail== memberEmail)
            {
                Staff_Window.SetInfo(meetingData);
                return;
            }
        }

        foreach (string memberEmail in meetingData.invite)
        {
            if (SysyemManager.SM.UserEmail == memberEmail)
            {
                Listener_Window.SetInfo(meetingData,Identity.Invited);
                return;
            }
        }

        foreach (string memberEmail in meetingData.require)
        {
            if (SysyemManager.SM.UserEmail == memberEmail)
            {
                Listener_Window.SetInfo(meetingData, Identity.Require);
                return;
            }
        }
        Listener_Window.SetInfo(meetingData, Identity.NotRequire);
    }
}
