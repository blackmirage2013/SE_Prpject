﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShot : MonoBehaviour
{
    public RenderTexture rt; //儲存所有要截圖的相機
    public string savePath; //儲存位置(不包含Application.dataPath)
    public void Screenshot()
    {
        RenderTexture.active = rt;
        Texture2D tex = new Texture2D(rt.width, rt.height, TextureFormat.RGB24, false);
        tex.ReadPixels(new Rect(0, 0, rt.width, rt.height), 0, 0);
        RenderTexture.active = null;

        byte[] bytes;
        bytes = tex.EncodeToPNG();

        string path = savePath + rt.name + ".png";
        System.IO.File.WriteAllBytes(path, bytes);
        Debug.Log(path);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Screenshot();
    }
}
