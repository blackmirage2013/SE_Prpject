﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomStateWindow : MonoBehaviour
{
    public Transform content;

    private void OnEnable()
    {
        for(int i = 0; i < RoomManager.instance.rooms.Count; i++)
        {
            Instantiate(RoomManager.instance.rooms[i], content);
        }
    }

    private void OnDisable()
    {
        for (int i = 0; i < content.childCount; i++)
        {
            Destroy(content.GetChild(i).gameObject);
        }
    }
}
