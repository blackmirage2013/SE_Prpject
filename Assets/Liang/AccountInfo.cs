﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[SerializeField]
public class AccountInfo : MonoBehaviour
{
    public Account account;

    public Text nameText;
    public Text mailText;

    public GameObject deleteButton;

    public void Init(Account _account)
    {
        account = _account;
        nameText.text = account.username;
        mailText.text = account.mail;
    }

    // 開啟/關閉刪除按鈕
    public void SwitchDeleteButton(bool state)
    {
        deleteButton.SetActive(state);
    }

    public void ClickToDelete()
    {
        if (AccountManager.instance.accountType == AccountType.RoomList)
            Destroy(this.gameObject);
        else if (AccountManager.instance.accountType == AccountType.CreateMeeting)
        {
            AccountManager.instance.createMeeting.RemoveInvite(this);
            Destroy(this.gameObject);
        }
        else if (AccountManager.instance.accountType == AccountType.Host)
        {
            AccountManager.instance.hostRoomInfo.RemoveUser(this);
            Destroy(this.gameObject);
        }
    }

    public void ClickToAdd()
    {
        if (AccountManager.instance.accountType == AccountType.RoomList)
            RoomManager.instance.AddMember(this);
        else if (AccountManager.instance.accountType == AccountType.CreateMeeting)
            AccountManager.instance.createMeeting.AddInvite(this);
        else if (AccountManager.instance.accountType == AccountType.Host)
            AccountManager.instance.hostRoomInfo.AddInvite(this);
    }

    public void Copy(AccountInfo _info)
    {
        account = _info.account;
        Init(account);
    }
}
