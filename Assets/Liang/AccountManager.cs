﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccountManager : MonoBehaviour
{
    static public AccountManager instance;

    // 所有已註冊的用戶
    public List<GameObject> accountList = new List<GameObject>();

    public GameObject accountPanel;
    public GameObject accountInfoPrefab;
    public Transform slots;

    public AccountType accountType;

    public CreateMeeting createMeeting;
    public HostRoomInfo hostRoomInfo;

    public DataLoader accountData; 

    private void Awake()
    {
        instance = this;       
    }
    void Start()
    {

        // 測試用資料
        //Account a1 = new Account("justin891204@hotmail.com.tw", "123", "梁又升");
        //AddNewAccount(a1);
        //Account a2 = new Account("mrpurplestar06@gmail.com", "123", "梁又升_1");
        //AddNewAccount(a2);
        //Account a3 = new Account("B10815048@gapps.ntust.edu.tw", "123", "梁又升_2");
        //AddNewAccount(a3);     
    }

    void Update()
    {
        
    }

    // 開啟用戶列表時要更新使用者的資料庫
    public void UpdateAccountList()
    {
        Debug.Log("幹");
        accountList.Clear();
        for(int i = 0; i < slots.childCount; i++)
        {
            Destroy(slots.GetChild(i).gameObject);
        }

        // 從資料庫取得已經註冊的使用者...
        Debug.Log(accountData.participators.Length);
        for (int i = 0; i < accountData.participators.Length - 1; i++)
        {
            //Debug.Log("length = " + accountData.participators.Length);
            Account account = new Account(accountData.GetMail(i), accountData.GetPwd(i), accountData.GetName(i));
            AddNewAccount(account);
        }

        // 範例: 
        // Account a1 = new Account("justin891204@hotmail.com.tw", "123", "梁又升");
        // AddNewAccount(a1);
    }

    public void OpenAccountList(bool state)
    {
        UpdateAccountList();
        accountPanel.SetActive(state);
    }

    public void AddNewAccount(Account _account)
    {
        GameObject info = Instantiate(accountInfoPrefab, slots);
        info.GetComponent<AccountInfo>().Init(_account);
        accountList.Add(info);
    }
    // 用mail來取得該成員的AccountInfo
    public AccountInfo GetAccountInfo(string _Email)
    {
        foreach (GameObject go in accountList)
        {
            if (go.GetComponent<AccountInfo>().mailText.text == _Email)
                return go.GetComponent<AccountInfo>();
        }
        Debug.LogError("沒抓到該成員!資料庫儲存錯誤");
        return null;
    }
    public void SetAccountType(AccountType _accountType)
    {
        accountType = _accountType;
    }
}

public class Account
{
    public Account(string _mail, string _password, string _username)
    {
        mail = _mail;
        password = _password;
        username = _username;
    }

    public string mail;
    public string password;
    public string username;

    public List<string> unreadMessage;
    public List<string> readMessage;
}
public enum AccountType
{
    RoomList,
    Host,
    CreateMeeting
}