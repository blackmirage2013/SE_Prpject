﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MeetingRoom : MonoBehaviour
{
    public int primaryIndex = 0;    // 給資料庫的檢索值

    public GameObject deleteButton;

    public Text roomNameText;

    public Image stateImg;
    public Text stateText;
    public Sprite free;
    public Sprite stop;

    public string roomName;

    // 是否開放會議室
    public bool openToUse = true;

    // 可以使用房間的成員
    public List<AccountInfo> members = new List<AccountInfo>();

    // 可使用的時間範圍 
    public int startHour;
    public int startMin;
    public int endHour;
    public int endMin;

    // 已預訂的會議資訊
    public List<MeetingData> meetingList = new List<MeetingData>();
    public List<MeetingData> MeetingList
    {
        get { return meetingList; }
        set { }
    }

    // 初始化
    public void Init()
    {
        //roomName = "Room " + UnityEngine.Random.Range(0, 10);
        //lowerTimeBound = System.DateTime.Now;
        //upperTimeBound = lowerTimeBound.AddDays(1);
        //roomNameText.text = roomName;
        //openToUse = true;
        members = new List<AccountInfo>();
    }

    // 更新房間的使用狀態 (只在儲存時呼叫)
    public void UpdateRoomInfoState()
    {
        roomNameText.text = roomName;
        if (openToUse)
        {
            stateImg.sprite = free;
            stateText.text = "可使用";
        }
        else
        {
            stateImg.sprite = stop;
            stateText.text = "使用中";
        }
        return;
    }

    // 定期更新用
    public void TimeToCheck()
    {
        bool roomAvailable = false;
        // 檢查當前時間
        if (DateTime.Now.Hour >= startHour && DateTime.Now.Minute >= startMin && DateTime.Now.Hour <= endHour && DateTime.Now.Minute <= endMin)
        {
            roomAvailable = true;
            // 檢查有沒有會議
            for (int i = 0; i < meetingList.Count; i++)
            {
                if ((DateTime.Now > meetingList[i].meetingTime.startTime && DateTime.Now < meetingList[i].meetingTime.endTime))
                {
                    roomAvailable = false;
                    break;
                }
            }
        }        

        if (roomAvailable)
        {
            stateImg.sprite = free;
            stateText.text = "可使用";            
        }
        else
        {
            stateImg.sprite = stop;
            stateText.text = "使用中";
        }
    }

    // 計算時間區間是否有會議 有的話回傳true
    public bool GetRoomState(MeetingData meetingData)
    {
        bool conflict = false;
        // 計算時間區間是否有其他會議
        for (int i = 0; i < meetingList.Count; i++)
        {
            if ((meetingData.meetingTime.startTime < meetingList[i].meetingTime.startTime && meetingData.meetingTime.endTime < meetingList[i].meetingTime.startTime) ||
                (meetingData.meetingTime.startTime > meetingList[i].meetingTime.endTime && meetingData.meetingTime.endTime > meetingList[i].meetingTime.endTime))
            {
                Debug.Log("沒有衝突");
            }
            else
            {
                conflict = true;
                Debug.Log("時間衝突");
            }
        }

        return conflict;
    }

    // 增加會議資訊到會議室 <從CreateMeeting加入>
    public bool AddMeetingData(MeetingData _meetingData)
    {
        //防呆 開始大於等於結束時間 或是開始時間比現在早
        if (_meetingData.meetingTime.startTime >= _meetingData.meetingTime.endTime || _meetingData.meetingTime.startTime < DateTime.Now)
        {
            Debug.LogWarning("輸入時間錯誤!");
            return false;
        }

        //檢查是否有權限開會議
        bool checkHost = false;
        for (int i = 0; i < members.Count; i++)
        {
            if(_meetingData.host == members[i].mailText.text)
            {
                checkHost = true;
            }
        }
        if(!checkHost)
        {
            Debug.LogWarning("主持人沒有權限創建會議");
            return false;
        }

        // 檢查所有會議室的時段
        foreach (var meetingData in meetingList)
        {
            if ((_meetingData.meetingTime.startTime <= meetingData.meetingTime.startTime && meetingData.meetingTime.startTime < _meetingData.meetingTime.endTime) ||
                (_meetingData.meetingTime.startTime < meetingData.meetingTime.endTime && meetingData.meetingTime.endTime <= _meetingData.meetingTime.endTime))
            {
                Debug.LogWarning("該時段已有人預約了!");
                return false;
            }
        }

        meetingList.Add(new MeetingData(_meetingData,SysyemManager.SM.UserEmail));
        Debug.Log("預約成功!");
        return true;
    }

    //移除會議
    public bool RemoveMeetingData(MeetingData _meetingData)
    {
        for (int i = 0; i < meetingList.Count; i++)//將指定會議移除
        {
            //Debug.Log("內容物"+roomDatas[i].meetings[j].meetingTime.startTime);
            if (meetingList[i].meetingTime.startTime == _meetingData.meetingTime.startTime)
            {
                //還需要加入成員通知
                meetingList.RemoveAt(i);
                Debug.Log("移除成功!");
                return true;
            }
        }
        return false;
    }

    //用會議開始時間找會議
    public MeetingData FindMeetingData(DateTime _startTime)
    {
        for (int i = 0; i < meetingList.Count; i++)
        {
            if (meetingList[i].meetingTime.startTime == _startTime)
            {
                return meetingList[i];
            }
        }
        return null;
    }

    //更改或設定會議
    public bool SetMeetingData(DateTime _startTime, MeetingData _meetingData)
    {
        for (int i = 0; i < meetingList.Count; i++)
        {
            if (meetingList[i].meetingTime.startTime == _startTime)
            {
                meetingList[i] = new MeetingData(_meetingData, _meetingData.host);
                return true;
            }
        }
        return false;
    }

    // 開啟/關閉刪除按鈕
    public void SwitchDeleteButton(bool state)
    {
        deleteButton.SetActive(state);
    }

    // 按下刪除按鈕
    public void ClickToDelete()
    {
        // 通知所有已經預約這個房間的成員
        // ...

        RoomManager.instance.DeleteRoom(this.gameObject);
    }

    public void ClickToShowInfo()
    {
        RoomManager.instance.UpdateRoomsInfo(this);
    }

    public void AddMember(AccountInfo newAccount)
    {
        members.Add(newAccount);
    }

    public void DeleteMember(AccountInfo account)
    {
        foreach(AccountInfo member in members)
        {
            if(member == account)
            {
                members.Remove(member);
                return;
            }
        }
    }
}
