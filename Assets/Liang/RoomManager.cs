﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public List<GameObject> rooms = new List<GameObject>();

    public GameObject roomInfoWindow;

    public GameObject roomInfoPrefab;
    public Transform roomInfoContext;

    public MeetingRoom currentMeetingRoom;
    public InputField roomInfo_Name_InputField;
    public Text roomInfo_Name;
    public Toggle roomInfo_Enable;

    public InputField roomInfo_StartHour_InputField;
    public InputField roomInfo_StartMin_InputField;
    public InputField roomInfo_EndHour_InputField;
    public InputField roomInfo_EndMin_InputField;

    public GameObject memberPrefab;
    public Transform memberContext;

    static public RoomManager instance;

    private bool enableButton_room = false;
    private bool enableButton_member = false;

    private string[] appointmentMemberMail;

    // 資料庫的最大檢索值
    private int maxPrimaryIndex = 0;

    public RoomInfoLoader roomInfoLoader;
    public DataLoader accountInfo;
    public CreateRoom createRoom;
    public UpdateRoom update_Room;
    public DeleteRoom delete_Room;
    public meetingInfoLoader meetingInfo;

    void Start()
    {
        instance = this;
        if(rooms.Count == 0)
        {
            roomInfoWindow.SetActive(false);
        }

        maxPrimaryIndex = roomInfoLoader.GetMaxIDNumber();
    }

    void Update()
    {
        
    }

    public void OnEnable()
    {
        AccountManager.instance.accountType = AccountType.RoomList;        
        UpdateRoomsList();
    }

    // 從資料庫更新會議室列表 (開啟面板時更新)
    public void UpdateRoomsList()
    {
        // 完成資料庫連結後把註解拿掉
        for (int i = 0; i < roomInfoContext.childCount; i++)
        {
            Destroy(roomInfoContext.GetChild(i).gameObject);
        }
        rooms.Clear();

        // 把資料庫的資料傳入... 生成rooms
        Debug.Log("rooms.Length - 1 = " + roomInfoLoader.rooms.Length);
        for(int i = 0; i < roomInfoLoader.rooms.Length - 1; i++)
        {
            GameObject prefab = Instantiate(roomInfoPrefab, roomInfoContext);
            prefab.GetComponent<MeetingRoom>().Init();
            rooms.Add(prefab);
            currentMeetingRoom = prefab.GetComponent<MeetingRoom>();
            currentMeetingRoom.roomName = roomInfoLoader.GetRoomname(i);
            currentMeetingRoom.startHour = roomInfoLoader.GetStarttime(i) / 60;
            currentMeetingRoom.startMin = roomInfoLoader.GetStarttime(i) % 60;
            currentMeetingRoom.endHour = roomInfoLoader.GetEndtime(i) / 60;
            currentMeetingRoom.endMin = roomInfoLoader.GetEndtime(i) % 60;
            currentMeetingRoom.openToUse = roomInfoLoader.GetRoomstate(i);
            currentMeetingRoom.primaryIndex = roomInfoLoader.GetID(i);
            currentMeetingRoom.UpdateRoomInfoState();

            string[] memberID = roomInfoLoader.GetMember_id(i).Split(',');
            for (int j = 0; j < memberID.Length - 1; j++)
            {
                for(int k = 0; k < accountInfo.participators.Length - 1; k++)
                {
                    if (memberID[j] == accountInfo.GetID(k))
                    {
                        //appointmentMemberMail[j] = accountInfo.GetMail(k);
                        currentMeetingRoom.members.Add(AccountManager.instance.GetAccountInfo(accountInfo.GetMail(k)));
                        break;
                    }
                }
            }
            // 把資料庫的成員預約名單資料用 AccountManager.instance.SearchAccountInfo(email) 轉成 AccountInfo
            //for (int j = 0; j < appointmentMemberMail.Length; j++)
            //{

            //}

            string[] meetingID = roomInfoLoader.GetMeetings_id(i).Split(',');
            // 更新會議室的會議資訊
            for (int j = 0; j < meetingID.Length - 1; j++)
            {
                for (int k = 0; k < meetingInfo.meetings.Length - 1; k++)
                {
                    if (meetingID[j] == meetingInfo.GetID(k))
                    {
                        DateTime startTime = Convert.ToDateTime(meetingInfo.GetStarttime(k));
                        DateTime endTime = Convert.ToDateTime(meetingInfo.GetEndtime(k));
                        MeetingTime time = new MeetingTime(startTime, endTime);

                        List<string> member = new List<string>();
                        List<string> invite = new List<string>();
                        List<string> require = new List<string>();
                        string[] member_ID = meetingInfo.GetMember(k).Split(',');
                        for (int m = 0; m < member_ID.Length - 1; m++)
                        {
                            for(int n = 0; n < accountInfo.participators.Length - 1; n++)
                            {
                                if (accountInfo.GetID(n) == member_ID[m])
                                {
                                    member.Add(accountInfo.GetMail(n));
                                    break;
                                }
                            }
                        }
                        string[] invite_ID = meetingInfo.GetInvite(k).Split(',');
                        for (int m = 0; m < invite_ID.Length - 1; m++)
                        {
                            for (int n = 0; n < accountInfo.participators.Length - 1; n++)
                            {
                                if (accountInfo.GetID(n) == invite_ID[m])
                                {
                                    invite.Add(accountInfo.GetMail(n));
                                    break;
                                }
                            }
                        }
                        string[] apply_ID = meetingInfo.GetApply(k).Split(',');
                        for (int m = 0; m < apply_ID.Length - 1; m++)
                        {
                            for (int n = 0; n < accountInfo.participators.Length - 1; n++)
                            {
                                if (accountInfo.GetID(n) == apply_ID[m])
                                {
                                    require.Add(accountInfo.GetMail(n));
                                    break;
                                }
                            }
                        }
                        currentMeetingRoom.meetingList.Add(new MeetingData(meetingInfo.GetMeetingname(k), meetingInfo.GetRoomname(k), meetingInfo.GetMeetinginfo(k), time, meetingInfo.GetMaxmemberamount(k), meetingInfo.GetHost(k), member, invite, require));
                        Debug.Log("HWLLO");
                    }
                }
            }
        }

        maxPrimaryIndex = roomInfoLoader.GetMaxIDNumber();
        // 範例:
        // GameObject prefab = Instantiate(roomInfoPrefab, roomInfoContext);
        // prefab.GetComponent<MeetingRoom>().Init();
        // rooms.Add(prefab);
        // currentMeetingRoom = prefab.GetComponent<MeetingRoom>();
        // currentMeetingRoom.roomName = 會議室名稱;
        // currentMeetingRoom.startHour = 開始時間(小時);
        // currentMeetingRoom.startMin = 開始時間(分鐘);
        // currentMeetingRoom.endHour = 結束時間(小時);
        // currentMeetingRoom.endMin = 結束時間(分鐘);
        // currentMeetingRoom.openToUse = 是否開放使用;
        // 把資料庫的成員預約名單資料用 AccountManager.instance.SearchAccountInfo(email) 轉成 AccountInfo
        // for(int i = 0; i < 成員名單陣列長度; i++)
        // {
        //      currentMeetingRoom.members.Add(AccountManager.instance.SearchAccountInfo(成員的email));
        // }
        // 更新會議室的會議資訊
        // for (int i = 0; i < 註冊會議的陣列長度; i++)
        // {
        //      currentMeetingRoom.meetingList.Add(new MeetingData(會議名稱, 會議室名稱, 會議資訊, 會議時間, 成員上限, 主持人的信箱, 已邀請的成員名單, 已參加的成員名單));
        // }

    }

    // 把新增的會議室資料儲存至資料庫
    public void InsertRoomData(MeetingRoom data)
    {
        int roomstate = 0;
        if (data.openToUse) roomstate = 1;
        string memberID = "";
        for(int i = 0; i < data.members.Count; i++)
        {
            for(int j = 0; j < accountInfo.participators.Length - 1; j++)
            {
                if (accountInfo.GetMail(j) == data.members[i].mailText.text)
                {
                    memberID += accountInfo.GetID(j);
                    memberID += ",";
                }
            }
        }
        string meetingsID = "";
        for(int i = 0; i < data.meetingList.Count; i++)
        {
            for(int j = 0; j < meetingInfo.meetings.Length - 1; j++)
            {
                DateTime start_time= Convert.ToDateTime(meetingInfo.GetStarttime(j));
                if (meetingInfo.GetMeetingname(j)==data.meetingList[i].meetingName && meetingInfo.GetRoomname(j) == data.meetingList[i].roomName && start_time == data.meetingList[i].meetingTime.startTime)
                {
                    meetingsID += meetingInfo.GetID(j);
                    meetingsID += ",";
                }
            }
        }
        StartCoroutine(createRoom.InsertRoom(data.primaryIndex, data.roomName, data.startHour * 60 + data.startMin, data.endHour * 60 + data.endMin, roomstate, memberID, meetingsID));
        Invoke("InvokeUpdateDataBase", 1.0f);
    }

    // 把會議室資料儲存至資料庫
    public void SaveRoomData(MeetingRoom data)
    {
        int roomstate = 0;
        if (data.openToUse) roomstate = 1;
        string memberID = "";
        for (int i = 0; i < data.members.Count; i++)
        {
            for (int j = 0; j < accountInfo.participators.Length - 1; j++)
            {
                if (accountInfo.GetMail(j) == data.members[i].mailText.text)
                {
                    memberID += accountInfo.GetID(j);
                    memberID += ",";
                }
            }
        }
        string meetingID = "";
        for (int j = 0; j < meetingInfo.meetings.Length - 1; j++)
        {
            if (meetingInfo.GetRoomname(j) == data.roomName)
            {
                meetingID += meetingInfo.GetID(j);
                meetingID += ",";
            }
        }
        Debug.Log("meetingID = " + meetingID);
        //string meetingsID = "";
        //Debug.Log(data.meetingList.Count + "??");
        //for (int i = 0; i < data.meetingList.Count; i++)
        //{
        //    Debug.Log(meetingInfo.meetings);
        //    for (int j = 0; j < meetingInfo.meetings.Length - 1; j++)
        //    {
        //        DateTime start_time = Convert.ToDateTime(meetingInfo.GetStarttime(j));
        //        if (meetingInfo.GetMeetingname(j) == data.meetingList[i].meetingName && meetingInfo.GetRoomname(j) == data.meetingList[i].roomName && start_time == data.meetingList[i].meetingTime.startTime)
        //        {
        //            Debug.Log("找到符合的會議資訊");
        //            meetingsID += meetingInfo.GetID(j);
        //            meetingsID += ",";
        //        }
        //    }
        //}
        Debug.Log("資料庫儲存");
        update_Room.UseStartcoroUpdateRoom(data.primaryIndex, data.roomName, data.startHour * 60 + data.startMin, data.endHour * 60 + data.endMin, roomstate, memberID, meetingID);
        //StartCoroutine(update_Room.updateRoom(data.primaryIndex, data.roomName, data.startHour * 60 + data.startMin, data.endHour * 60 + data.endMin, roomstate, memberID, meetingsID));
        Invoke("InvokeUpdateDataBase", 1.0f);
    }

    public void DelaySave(MeetingRoom data)
    {
        Invoke("SaveRoomData", 1f);
    }

    // 刪除指定資料庫的會議室
    public void DeleteRoomData(MeetingRoom data)
    {
        StartCoroutine(delete_Room.deleteRoom(data.primaryIndex));
        Invoke("InvokeUpdateDataBase", 1.0f);
    }

    // 按下房間列表的按鈕
    public void UpdateRoomsInfo(MeetingRoom meetingRoom)
    {
        currentMeetingRoom = meetingRoom;
        roomInfo_Name_InputField.text = meetingRoom.roomName;
        roomInfo_StartHour_InputField.text = currentMeetingRoom.startHour.ToString();
        roomInfo_StartMin_InputField.text = currentMeetingRoom.startMin.ToString();
        roomInfo_EndHour_InputField.text = currentMeetingRoom.endHour.ToString();
        roomInfo_EndMin_InputField.text = currentMeetingRoom.endMin.ToString();
        roomInfo_Enable.isOn = meetingRoom.openToUse;
        for(int i = 0; i < memberContext.childCount; i++)
        {
            Destroy(memberContext.GetChild(i).gameObject);
        }
        
        for (int i = 0; i < meetingRoom.members.Count; i++)
        {          
            GameObject go = Instantiate(memberPrefab, memberContext);
            go.GetComponent<AccountInfo>().Copy(meetingRoom.members[i]);
        }
    }

    // 保存
    public void SaveRoomInfo()
    {
        bool createNew = false;

        if(!CheckTime())
        {
            return;
        }
        if (!CheckRoomName(roomInfo_Name_InputField.text))
        {
            Debug.Log("名稱欄位為空或是衝突");
            return;
        }           

        // 這是要新創的房間
        if (currentMeetingRoom == null)
        {           
            GameObject prefab = Instantiate(roomInfoPrefab, roomInfoContext);
            prefab.GetComponent<MeetingRoom>().Init();
            rooms.Add(prefab);
            currentMeetingRoom = prefab.GetComponent<MeetingRoom>();

            // 新創的房間鍵值=資料庫最大值+1
            currentMeetingRoom.primaryIndex = ++maxPrimaryIndex;
            Debug.Log("maxID = " + maxPrimaryIndex );

            createNew = true;
        }

        currentMeetingRoom.roomName = roomInfo_Name_InputField.text;
        currentMeetingRoom.startHour = int.Parse(roomInfo_StartHour_InputField.text);
        currentMeetingRoom.startMin = int.Parse(roomInfo_StartMin_InputField.text);
        currentMeetingRoom.endHour = int.Parse(roomInfo_EndHour_InputField.text);
        currentMeetingRoom.endMin = int.Parse(roomInfo_EndMin_InputField.text);
        currentMeetingRoom.openToUse = roomInfo_Enable.isOn;
        currentMeetingRoom.UpdateRoomInfoState();
        currentMeetingRoom.members.Clear();
        for (int i = 0; i < memberContext.childCount; i++)
        {
            currentMeetingRoom.members.Add(memberContext.GetChild(i).gameObject.GetComponent<AccountInfo>());
        }
        if(createNew)
        {
            InsertRoomData(currentMeetingRoom);
        }
        else
        {
            SaveRoomData(currentMeetingRoom);
        }
        Debug.Log("儲存成功!");

        // 把儲存的資料更新到資料庫...

    }

    private void InvokeUpdateDataBase()
    {
        roomInfoLoader.UseStartcoroUpdate();
    }

    private bool CheckRoomName(string name)
    {
        if (name == "")
            return false;

        for(int i = 0; i < rooms.Count; i++)
        { 
            if (name == rooms[i].GetComponent<MeetingRoom>().roomName)
                if(currentMeetingRoom == null || currentMeetingRoom.gameObject != rooms[i])
                    return false;
        }

        return true;
    }

    private bool CheckTime()
    {
        if(roomInfo_StartHour_InputField.text == "" || roomInfo_StartMin_InputField.text == "" || roomInfo_EndHour_InputField.text == "" || roomInfo_EndMin_InputField.text == "")
        {
            Debug.Log("時間欄位為空");
            return false;
        }
           
        if(int.Parse(roomInfo_StartHour_InputField.text) > 23 || int.Parse(roomInfo_StartMin_InputField.text) > 59 || int.Parse(roomInfo_EndHour_InputField.text) > 23 || int.Parse(roomInfo_EndMin_InputField.text) > 59)
        {
            Debug.Log("時間輸入錯誤");
            return false;
        }

        if(int.Parse(roomInfo_StartHour_InputField.text) * 60 + int.Parse(roomInfo_StartMin_InputField.text) >= int.Parse(roomInfo_EndHour_InputField.text) * 60 + int.Parse(roomInfo_EndMin_InputField.text))
        {
            Debug.Log("時間開始和結束衝突");
            return false;
        }

        return true;
    }

    // 取消
    public void CancelRoomInfo()
    {       
        if(currentMeetingRoom != null)
            UpdateRoomsInfo(currentMeetingRoom);
    }

    public void ClickToAddRoom()
    {
        InitNewRoomInfo();
        //GameObject prefab = Instantiate(roomInfoPrefab, roomInfoContext);
        //prefab.GetComponent<MeetingRoom>().Init();
        //rooms.Add(prefab);
        //if (rooms.Count == 1)
        //{
        //    UpdateRoomsInfo(prefab.GetComponent<MeetingRoom>());
        //}
        //roomInfoWindow.SetActive(true);
    }

    public void InitNewRoomInfo()
    {
        roomInfoWindow.SetActive(true);
        currentMeetingRoom = null;
        roomInfo_Name_InputField.text = "";
        roomInfo_Enable.isOn = true;
        roomInfo_StartHour_InputField.text = "0";
        roomInfo_StartMin_InputField.text = "0";
        roomInfo_EndHour_InputField.text = "0";
        roomInfo_EndMin_InputField.text = "0";
        for (int i = 0; i < memberContext.childCount; i++)
        {
            Destroy(memberContext.GetChild(i).gameObject);
        }
    }

    public void ClickToEditRoom()
    {
        enableButton_room = !enableButton_room;
        for(int i = 0; i < rooms.Count; i++)
        {
            rooms[i].GetComponent<MeetingRoom>().SwitchDeleteButton(enableButton_room);
        }
    }

    public void DeleteRoom(GameObject room)
    {
        foreach(GameObject go in rooms)
        {
            if(go == room)
            {
                DeleteRoomData(room.GetComponent<MeetingRoom>());

                rooms.Remove(room);

                if (rooms.Count == 0)
                {
                    roomInfoWindow.SetActive(false);
                }
                else if (room == currentMeetingRoom.gameObject)
                {
                    UpdateRoomsInfo(rooms[0].GetComponent<MeetingRoom>());
                }

                Destroy(room);
                break;
            }
        }
    }

    public void ClickToAddMember()
    {
        AccountManager.instance.OpenAccountList(true);
    }

    public void AddMember(AccountInfo info)
    {
        // 檢查是否重複
        for(int i = 0; i < memberContext.childCount; i++)
        {
            if(memberContext.GetChild(i).GetComponent<AccountInfo>().account.mail == info.account.mail)
            {
                return;
            }
        }

        GameObject memberInfo = Instantiate(memberPrefab, memberContext);
        memberInfo.GetComponent<AccountInfo>().Copy(info);
    }

    public void ClickToEditMember()
    {
        enableButton_member = !enableButton_member;
        for (int i = 0; i < memberContext.childCount; i++)
        {
            memberContext.GetChild(i).GetComponent<AccountInfo>().SwitchDeleteButton(enableButton_member);
        }
    }
    public MeetingRoom FindRoomWithName(string _roomName)
    {
        for (int i = 0; i < rooms.Count; i++)
        {
            if (rooms[i].GetComponent<MeetingRoom>().roomName == _roomName)
            {
                return rooms[i].GetComponent<MeetingRoom>();
            }
        }
        return null;
    }
    
}
