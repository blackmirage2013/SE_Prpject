﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RowInfo : MonoBehaviour
{
    public MeetingRoom meetingRoom;
    public Text nameText;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Init(MeetingRoom _meetingroom)
    {
        meetingRoom = _meetingroom;
        nameText.text = meetingRoom.roomName;
    }
}
